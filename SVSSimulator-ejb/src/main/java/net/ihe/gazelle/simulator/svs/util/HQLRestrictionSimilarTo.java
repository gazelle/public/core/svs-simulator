package net.ihe.gazelle.simulator.svs.util;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.beans.HQLRestrictionValues;

public class HQLRestrictionSimilarTo implements HQLRestriction {

	private String path;
	private String regex;
	private String escapeCharacters;

	public HQLRestrictionSimilarTo(String path, String regex, String escapeCharacters) {
		super();
		this.path = path;
		this.regex = regex;
		this.setEscapeCharacters(escapeCharacters);
	}

	public HQLRestrictionSimilarTo(String path, String regex) {
		super();
		this.path = path;
		this.regex = regex;
	}

	@Override
	public void toHQL(HQLQueryBuilder<?> queryBuilder, HQLRestrictionValues values, StringBuilder sb) {
		if (path != null && regex != null) {
			//sb.append("regexp(");
			sb.append(queryBuilder.getShortProperty(path));
			sb.append(" like '%");
			sb.append(regex);
			sb.append("%'");
		}
	}

	public String getEscapeCharacters() {
		return escapeCharacters;
	}

	public void setEscapeCharacters(String escapeCharacters) {
		this.escapeCharacters = escapeCharacters;
	}
}
