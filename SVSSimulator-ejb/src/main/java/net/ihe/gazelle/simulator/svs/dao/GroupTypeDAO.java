package net.ihe.gazelle.simulator.svs.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.svs.DescribedValueSet;
import net.ihe.gazelle.svs.GroupType;

import java.util.Map;

public interface GroupTypeDAO {

    Filter<GroupType> getFilterGroup();
    void setFilterGroup(Filter<GroupType> filterGroup);
    FilterDataModel<GroupType> getAllGroups();
    GroupType getGroupTypeFromUrl(Map<String,String> urlParams);
    HQLCriterionsForFilter<GroupType> getHQLCriteriaForFilterGroup();

    String saveGroupAndRedirect(GroupType group);
    String saveEditGroupAndRedirect(GroupType group);
    void saveEditGroup(GroupType group);
    void addValueSetToGroup(DescribedValueSet valueSet, GroupType group);
    void removeValueSetToGroup(DescribedValueSet valueSet, GroupType group);
    String deleteGroup(GroupType group);

}
