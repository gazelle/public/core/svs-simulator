package net.ihe.gazelle.simulator.svs.action;

import net.ihe.gazelle.svs.DescribedValueSet;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;
import java.io.Serializable;

@BypassInterceptors
@Name("describedValueSetConverter")
@Converter(forClass = DescribedValueSet.class)
public class DescribedValueSetConverter implements javax.faces.convert.Converter, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DescribedValueSetConverter.class);

    private static final long serialVersionUID = 739049097980035837L;

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value instanceof DescribedValueSet) {
            DescribedValueSet describedValueSet = (DescribedValueSet) value;
            if (describedValueSet.getId() == null) {
                return null;
            }
            return describedValueSet.getId().toString();
        } else {
            return null;
        }

    }


    @Transactional
    public Object getAsObject(FacesContext Context,
                              UIComponent Component, String value) throws ConverterException {
        if (value != null) {
            try {
                Integer id = Integer.parseInt(value);
                if (id != null) {
                    EntityManager entityManager = (EntityManager) org.jboss.seam.Component.getInstance("entityManager");
                    DescribedValueSet describedValueSet = entityManager.find(DescribedValueSet.class, id);

                    return describedValueSet;

                }
            } catch (NumberFormatException e) {
                LOG.error("" + e.getMessage());
            }
        }
        return null;
    }
}
