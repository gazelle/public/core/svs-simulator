package net.ihe.gazelle.simulator.svs.util;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.FileTypeMap;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by gthomazon on 21/06/17.
 */
public class DocumentFileUpload {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentFileUpload.class);

    public static void showFile(String fullPath) {
        showFile((String) fullPath, (String) null, false);
    }

    public static void showFile(String fullPath, String filename, boolean download) {
        try {
            java.io.File f = new java.io.File(fullPath);
            if (filename == null) {
                filename = f.getName();
            }

            InputStream inputStream = null;
            if (f.exists()) {
                inputStream = new FileInputStream(fullPath);
            }

            showFile((InputStream) inputStream, filename, download);
            inputStream.close();
        } catch (Exception var5) {
            LOG.error("Impossible to display file : " + var5.getMessage());
        }

    }

    public static void showFile(byte[] bytes, String filename, boolean download) throws IOException {
        showFile((InputStream) (new ByteArrayInputStream(bytes)), filename, download);
    }

    public static void showFile(InputStream inputStream, String filename, boolean download) throws IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

        try {
            int j = 0;
            while (true) {
                response = (HttpServletResponse) PropertyUtils.getProperty(response, "response");
                j++;
                if (j  == Integer.MIN_VALUE) {
                    break;
                }
            }
        } catch (Exception var7) {
            response.reset();
            HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
            showFile(request, response, inputStream, filename, download);
            inputStream.close();
            facesContext.responseComplete();
        }
    }

    public static void showFile(HttpServletRequest request, HttpServletResponse response, InputStream inputStream, String filename, boolean download) {
        try {
            if (inputStream != null) {
                String userAgent = request.getHeader("user-agent");
                boolean isInternetExplorer = userAgent.indexOf("MSIE") > -1;
                int length = inputStream.available();
                if (filename != null && filename.toLowerCase().endsWith(".pdf")) {
                    response.setContentType("application/pdf");
                } else if (filename != null && filename.toLowerCase().endsWith(".xml")) {
                    response.setContentType("text/xml");
                } else {
                    String contentType = FileTypeMap.getDefaultFileTypeMap().getContentType(filename);
                    response.setContentType(contentType);
                }

                byte[] fileNameBytes = filename.getBytes(isInternetExplorer ? "windows-1250" : "utf-8");
                String dispositionFileName = "";
                byte[] var10 = fileNameBytes;
                int var11 = fileNameBytes.length;

                for (int var12 = 0; var12 < var11; ++var12) {
                    byte b = var10[var12];
                    dispositionFileName = dispositionFileName + (char) (b & 255);
                }

                String disposition;
                if (download) {
                    disposition = "attachment; filename=\"" + dispositionFileName + "\"";
                } else {
                    disposition = "inline; filename=\"" + dispositionFileName + "\"";
                }

                response.setHeader("Content-disposition", disposition);
                response.setContentLength(length);
                ServletOutputStream servletOutputStream = response.getOutputStream();
                IOUtils.copy(inputStream, servletOutputStream);
                servletOutputStream.flush();
                servletOutputStream.close();
                inputStream.close();
            }
        } catch (Exception var14) {
            LOG.error("Impossible to display file : " + var14.getMessage());
        }

    }
}
