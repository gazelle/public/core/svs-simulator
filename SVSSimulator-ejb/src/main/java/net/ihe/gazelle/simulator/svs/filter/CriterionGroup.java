package net.ihe.gazelle.simulator.svs.filter;

import net.ihe.gazelle.hql.criterion.PropertyCriterion;
import net.ihe.gazelle.svs.DescribedValueSet;
import net.ihe.gazelle.svs.GroupType;

public class CriterionGroup extends PropertyCriterion<DescribedValueSet, GroupType> {

	public CriterionGroup() {
		super(GroupType.class, "groupType", "this", "group");
	}

	@Override
	public String getSelectableLabelNotNull(GroupType groupType) {
		return groupType.getDisplayName();
	}

}
