package net.ihe.gazelle.simulator.svs.dao;

import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.datatypes.CEQuery;
import net.ihe.gazelle.svs.ConceptListType;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.EntityManager;

@Name("ceDAO")
public class CEDAOImpl implements CEDAO {

    private EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");

    /**
     * Save a concept in DB
     *
     * @param concept Concept to be saved
     */
    public void saveConcept(CE concept) {
        entityManager.merge(concept);
        entityManager.flush();
    }

    /**
     * Retrieve a specific concept from DB with a certain code
     *
     * @param code Code to identify the concept to be retrieved from DB
     */
    public CE getConceptFromCode(String code) {
        CEQuery query = new CEQuery(entityManager);
        query.code().eq(code);
        CE concept = query.getUniqueResult();
        if (concept != null) {
            return concept;
        } else {
            return null;
        }
    }

    /**
     * Add a concept to a conceptListType and persist concept
     *
     * @param conceptListType ConceptListType where concept has to be added
     * @param concept Concept to be added
     */
    public ConceptListType addConceptToConceptList(ConceptListType conceptListType, CE concept) {
        if (conceptListType != null && concept != null && !conceptListType.getConcept().contains(concept)) {
            conceptListType.addConcept(concept);
            entityManager.persist(concept);
            entityManager.flush();
        }
        return conceptListType;
    }

    /**
     * Delete a concept to a conceptListType and persist concept
     *
     * @param conceptListType ConceptListType where concept has to be deleted
     * @param concept Concept to be deleted
     */
    public ConceptListType deleteConceptFromConceptList(ConceptListType conceptListType, CE concept) {
        if (conceptListType != null && !conceptListType.getConcept().isEmpty() && concept != null && conceptListType.getConcept().contains(concept)) {
            conceptListType.removeConcept(concept);
            entityManager.remove(concept);
            entityManager.flush();
        }
        return conceptListType;
    }
}
