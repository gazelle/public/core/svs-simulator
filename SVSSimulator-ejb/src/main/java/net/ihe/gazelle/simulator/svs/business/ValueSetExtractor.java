package net.ihe.gazelle.simulator.svs.business;

import net.ihe.gazelle.common.util.ZipUtility;
import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.svs.action.XSDProviderLocal;
import net.ihe.gazelle.simulator.svs.dao.*;
import net.ihe.gazelle.simulator.svs.util.DocumentFileUpload;
import net.ihe.gazelle.svs.*;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.svs.GazelleSVSValidator;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.In;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ValueSetExtractor {

    private static Logger LOG = LoggerFactory.getLogger(ValueSetExtractor.class);

    private DescribedValueSet retrievedValueSetFromDB;

    private List<DescribedValueSet> newExtractedValueSetList = new ArrayList<>();
    private List<ConceptListType> newExtractedConceptTypeList = new ArrayList<>();
    private List<UpdatedDescribedValueSet> updatedExtractedValueSetList = new ArrayList<>();
    List<CE> addedConceptList = new ArrayList<>();
    List<String> updatedConceptList = new ArrayList<>();
    List<CE> deletedConceptList = new ArrayList<>();
    private List<DescribedValueSet> upToDateExtractedValueSetList = new ArrayList<>();
    private List<ConceptListType> upToDateExtractedConceptList = new ArrayList<>();

    private GazelleSVSValidator.ValidatorType validatorType;

    private Boolean updateRequestedAfterImport;
    private Boolean deleteRequestedAfterImport;

    private Boolean immediateImportInDB;

    public ValueSetExtractor() {
    }

    public DescribedValueSet getRetrievedValueSetFromDB() {
        return retrievedValueSetFromDB;
    }

    public void setRetrievedValueSetFromDB(DescribedValueSet retrievedValueSetFromDB) {
        this.retrievedValueSetFromDB = retrievedValueSetFromDB;
    }

    public List<DescribedValueSet> getNewExtractedValueSetList() {
        return newExtractedValueSetList;
    }

    public void setNewExtractedValueSetList(List<DescribedValueSet> newExtractedValueSetList) {
        this.newExtractedValueSetList = newExtractedValueSetList;
    }

    public List<ConceptListType> getNewExtractedConceptTypeList() {
        return newExtractedConceptTypeList;
    }

    public void setNewExtractedConceptTypeList(List<ConceptListType> newExtractedConceptTypeList) {
        this.newExtractedConceptTypeList = newExtractedConceptTypeList;
    }

    public List<UpdatedDescribedValueSet> getUpdatedExtractedValueSetList() {
        return updatedExtractedValueSetList;
    }

    public void setUpdatedExtractedValueSetList(List<UpdatedDescribedValueSet> updatedExtractedValueSetList) {
        this.updatedExtractedValueSetList = updatedExtractedValueSetList;
    }

    public List<CE> getAddedConceptList() {
        return addedConceptList;
    }

    public void setAddedConceptList(List<CE> addedConceptList) {
        this.addedConceptList = addedConceptList;
    }

    public List<String> getUpdatedConceptList() {
        return updatedConceptList;
    }

    public void setUpdatedConceptList(List<String> updatedConceptList) {
        this.updatedConceptList = updatedConceptList;
    }

    public List<CE> getDeletedConceptList() {
        return deletedConceptList;
    }

    public void setDeletedConceptList(List<CE> deletedConceptList) {
        this.deletedConceptList = deletedConceptList;
    }

    public List<DescribedValueSet> getUpToDateExtractedValueSetList() {
        return upToDateExtractedValueSetList;
    }

    public void setUpToDateExtractedValueSetList(List<DescribedValueSet> upToDateExtractedValueSetList) {
        this.upToDateExtractedValueSetList = upToDateExtractedValueSetList;
    }

    public List<ConceptListType> getUpToDateExtractedConceptList() {
        return upToDateExtractedConceptList;
    }

    public void setUpToDateExtractedConceptList(List<ConceptListType> upToDateExtractedConceptList) {
        this.upToDateExtractedConceptList = upToDateExtractedConceptList;
    }

    public GazelleSVSValidator.ValidatorType getValidatorType() {
        return validatorType;
    }

    public void setValidatorType(GazelleSVSValidator.ValidatorType validatorType) {
        this.validatorType = validatorType;
    }

    public Boolean getUpdateRequestedAfterImport() {
        return updateRequestedAfterImport;
    }

    public void setUpdateRequestedAfterImport(Boolean updateRequestedAfterImport) {
        this.updateRequestedAfterImport = updateRequestedAfterImport;
    }

    public Boolean getDeleteRequestedAfterImport() {
        return deleteRequestedAfterImport;
    }

    public void setDeleteRequestedAfterImport(Boolean deleteRequestedAfterImport) {
        this.deleteRequestedAfterImport = deleteRequestedAfterImport;
    }

    public Boolean getImmediateImportInDB() {
        return immediateImportInDB;
    }

    public void setImmediateImportInDB(Boolean immediateImportInDB) {
        this.immediateImportInDB = immediateImportInDB;
    }

    /**
     * Unzip a zip and put the extract files in the directory specified
     *
     * @param filePath      The zip file path
     * @param directoryPath The directory to destination path
     */
    public static void unzipFile(String filePath, String directoryPath) {

        FileInputStream fis = null;
        try {
            BufferedOutputStream dest = null;
            ZipInputStream zis = null;
            try {
                fis = new FileInputStream(filePath);
                try {
                    zis = new ZipInputStream(new BufferedInputStream(fis));
                    ZipEntry entry;
                    while ((entry = zis.getNextEntry()) != null) {
                        int count;
                        byte data[] = new byte[2048];
                        // write the files to the disk
                        File dirs = new File(directoryPath + "/" + entry.getName()).getParentFile();
                        if (!dirs.exists()) {
                            dirs.mkdirs();
                        }
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(directoryPath + "/" + entry.getName());
                            try {
                                dest = new BufferedOutputStream(fos, 2048);
                                while ((count = zis.read(data, 0, 2048)) != -1) {
                                    dest.write(data, 0, count);
                                }
                                dest.flush();
                                dest.close();
                            } catch (FileNotFoundException e1) {
                                LOG.error(e1.getMessage());
                            } catch (IOException e) {
                                FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                                LOG.error(e.getMessage(), e);
                                if (dest != null) {
                                    try {
                                        dest.close();
                                    } catch (IOException e1) {
                                        LOG.warn("The system has not been able to close a buffered output stream", e1);
                                    }
                                }
                            } finally {
                                try {
                                    if (dest != null) {
                                        dest.close();
                                    }
                                } catch (IOException e) {
                                    LOG.error("Error while closing dest : " + e.getMessage());
                                }
                            }
                            fos.close();
                        } catch (FileNotFoundException e1) {
                            LOG.error(e1.getMessage());
                        } catch (IOException e) {
                            FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                            LOG.error(e.getMessage(), e);
                            if (fos != null) {
                                try {
                                    fos.close();
                                } catch (IOException e1) {
                                    LOG.warn("The system has not been able to close a file output stream", e1);
                                }
                            }
                        } finally {
                            try {
                                if (fos != null) {
                                    fos.close();
                                }
                            } catch (IOException e) {
                                LOG.error("Error while closing fos : " + e.getMessage());
                            }
                        }
                    }
                    zis.close();
                } catch (FileNotFoundException e1) {
                    LOG.error(e1.getMessage());
                } catch (IOException e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                    LOG.error(e.getMessage(), e);
                    if (zis != null) {
                        try {
                            zis.close();
                        } catch (IOException e1) {
                            LOG.warn("The system has not been able to close a zip input stream", e1);
                        }
                    }
                } finally {
                    try {
                        if (zis != null) {
                            zis.close();
                        }
                    } catch (IOException e) {
                        LOG.error("Error while closing zis : " + e.getMessage());
                    }
                }
                fis.close();
            } catch (FileNotFoundException e1) {
                LOG.error(e1.getMessage());
            } catch (IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                LOG.error(e.getMessage(), e);
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e1) {
                        LOG.warn("The system has not been able to close a file input stream", e1);
                    }
                }
            } finally {
                try {
                    if (fis != null) {
                        fis.close();
                    }
                } catch (IOException e) {
                    LOG.error("Error while closing fis : " + e.getMessage());
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }


    /**
     * Transform uploaded zip file in DescribedValueSet, and save in BDD
     *
     * @throws IOException ioe
     */
    public void zipToObject(Boolean updateRequested, Boolean deleteRequested, Map<String,byte[]> zipFile, Map<String,byte[]> filesContent, Boolean immediateImportInDB) throws IOException {
        for (Map.Entry<String, byte[]> entry : zipFile.entrySet()) {
            String fileName = entry.getKey();
            // Create the File object for the uploaded zip file
            File file = File.createTempFile("tempZip", ".zip");
            FileUtils.writeByteArrayToFile(file, zipFile.get(fileName));

            // Create a temporary File for the unzip zip file
            File extractTo = createTempDir();

            // Extract the xml files in the temporary directory
            unzipFile(file.getAbsolutePath(), extractTo.getAbsolutePath());

            // Get All the extract files
            extractTo = new File(extractTo.getAbsolutePath() + "/");
            File[] filesExtract = extractTo.listFiles();

            if (filesContent == null) {
                filesContent = new HashMap<String, byte[]>();
            }
            // Add each files in the filesContent var
            //FIXME : the following code does not work if there are directories inside directories in the zip file.
            if (filesExtract != null) {
                for (File f : filesExtract) {
                    if (f.isDirectory()) {
                        File[] listOfFileInDirectory = f.listFiles();
                        if (listOfFileInDirectory != null) {
                            for (File ftemp : listOfFileInDirectory) {
                                String temp = FileUtils.readFileToString(ftemp.getAbsoluteFile());
                                filesContent.put(ftemp.getAbsolutePath(), temp.getBytes(StandardCharsets.UTF_8));
                            }
                        }
                    } else {
                        String temp = FileUtils.readFileToString(f.getAbsoluteFile());
                        filesContent.put(f.getAbsolutePath(), temp.getBytes(StandardCharsets.UTF_8));
                    }
                }
            }

            // Transform each xml in filesContent in object to save DB
            xmlToObject(updateRequested, deleteRequested, filesContent, immediateImportInDB);

            // Delete each files/directory created during the process
            FileUtils.deleteDirectory(extractTo.getAbsoluteFile());
            FileUtils.forceDelete(file.getAbsoluteFile());
        }
    }


    /**
     * Transform uploaded files in DescribedValueSet, and save in BDD
     */
    public void xmlToObject(Boolean updateRequested, Boolean deleteRequested, Map<String,byte[]> filesContent, Boolean immediateImportInDB) {

        int count = 1; // Counter nb. files
        for (Map.Entry<String, byte[]> entry : filesContent.entrySet()) {
            String fileName = entry.getKey();
            try {
                String fileContent = new String(filesContent.get(fileName), StandardCharsets.UTF_8);
                setValidatorTypeAndValidate(fileContent, updateRequested, deleteRequested, immediateImportInDB);
                count++;
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }
    }

    public void setValidatorTypeAndValidate(String fileContent, Boolean updateRequested, Boolean deleteRequested, Boolean immediateImportInDB) throws Exception {
        setUpdateRequestedAfterImport(updateRequested);
        setDeleteRequestedAfterImport(deleteRequested);
        setImmediateImportInDB(immediateImportInDB);
        resetLists();
        // Get the content and the type
        String returnXmlType = returnXmlType(fileContent);
        if (returnXmlType.equalsIgnoreCase("RetrieveValueSetResponse")) {
            // validate xml file
            setValidatorType(GazelleSVSValidator.ValidatorType.ITI48_RESPONSE);
            validateDocument(fileContent);
        } else if (returnXmlType.equalsIgnoreCase("RetrieveMultipleValueSetsResponse")) {
            setValidatorType(GazelleSVSValidator.ValidatorType.ITI60_RESPONSE);
            validateDocument(fileContent);
        } else {
            FacesMessages
                    .instance()
                    .add("The document doesn't seem to be part of this list: RetrieveValueSetResponse, RetrieveValueSetRequest, RetrieveMultipleValueSetsResponse, RetrieveMultipleValueSetsRequest.");
        }
    }

    /**
     * Validate imported file
     *
     * @param fileContent String content of the imported file
     */
    private void validateDocument(String fileContent) throws Exception {
        XSDProviderLocal xsdProvider = (XSDProviderLocal) Component.getInstance("xsdProvider");
        GazelleSVSValidator validator = new GazelleSVSValidator(fileContent,
                xsdProvider.getSchema(validatorType));
        DetailedResult dr = validator.validate(validatorType);
        if (dr.getValidationResultsOverview().getValidationTestResult().equalsIgnoreCase("PASSED")
                || ApplicationConfiguration.getBooleanValue("ignore_validation_in_import")) {
            checkTransactionTypeAndCompare(fileContent);
        }

        if (dr.getDocumentWellFormed() != null && dr.getDocumentWellFormed().getResult().equals("FAILED")) {
        } else if (dr.getDocumentValidXSD() != null
                && dr.getDocumentValidXSD().getResult().equalsIgnoreCase("FAILED")) {
            for (XSDMessage message : dr.getDocumentValidXSD().getXSDMessage()) {
            }
        } else if (dr.getMDAValidation() != null
                && dr.getMDAValidation().getResult().equalsIgnoreCase("FAILED")) {
        }
    }
    /**
     * Check transactionType by marshalling imported file and launch corresponding comparison method
     *
     * @param fileContent String content of the imported file
     */
    private void checkTransactionTypeAndCompare(String fileContent) {
        DescribedValueSetDAOImpl describedValueSetDAOImpl = new DescribedValueSetDAOImpl();
        ValueSetResponseType vsXml = null;
        DescribedValueSet extractedValueSet = null;
        List<DescribedValueSet> extractedValueSetList = new ArrayList<>();
        try {
            // Recup DescribedValueSet
            ByteArrayInputStream bais = new ByteArrayInputStream(fileContent.getBytes(StandardCharsets.UTF_8));
            if (validatorType.equals(GazelleSVSValidator.ValidatorType.ITI48_RESPONSE)) {
                RetrieveValueSetResponseType objectRecup = unmarshallDocument(
                        RetrieveValueSetResponseType.class, bais);
                vsXml = objectRecup.getValueSet();
                extractedValueSet = new DescribedValueSet(vsXml);
            } else if (validatorType.equals(GazelleSVSValidator.ValidatorType.ITI60_RESPONSE)) {
                RetrieveMultipleValueSetsResponseType objectRecup = (RetrieveMultipleValueSetsResponseType) unmarshallDocument(
                        RetrieveMultipleValueSetsResponseType.class, bais);
                extractedValueSetList = objectRecup.getDescribedValueSet();
            }
            bais.close();
        } catch (Exception e) {
            FacesMessages
                    .instance()
                    .add("Error in the document: Can't create the Value Set.");
        }
        if (validatorType.equals(GazelleSVSValidator.ValidatorType.ITI48_RESPONSE)) {
            if (extractedValueSet != null) {
                compareValueSetWithDBForITI48(extractedValueSet, describedValueSetDAOImpl);
            }
        } else {
            if (extractedValueSetList != null) {
                compareValueSetListWithDBForITI60(extractedValueSetList, describedValueSetDAOImpl);
            }
        }
    }

    /**
     * Compare Value Set retrieved from imported file to detect whether it is a new one or comparison process is needed
     * for a Single Value Set Response of ITI-48 transaction
     *
     * @param extractedValueSet Value Set from imported file
     * @param describedValueSetDAOImpl Implementation of DescribedValueSetDAO
     */
    private void compareValueSetWithDBForITI48(DescribedValueSet extractedValueSet, DescribedValueSetDAOImpl describedValueSetDAOImpl) {
        retrievedValueSetFromDB = describedValueSetDAOImpl.getValueSetfromDB(extractedValueSet);
        if (retrievedValueSetFromDB == null) {
            if (extractedValueSet.getConceptList() != null && !extractedValueSet.getConceptList().isEmpty()) {
                setMainListToConceptListAtCreation(extractedValueSet);
            }
            newExtractedValueSetList.add(extractedValueSet);
            addConceptListTypeToExtractedConceptTypeList(extractedValueSet, true);
            if (immediateImportInDB != null && immediateImportInDB) {
                describedValueSetDAOImpl.treatDescribedValueSetImportedFromRetrieveValueSetResponse(extractedValueSet);
            }
        } else {
            if (updateRequestedAfterImport != null && updateRequestedAfterImport) {
                compareConceptListWithDB(extractedValueSet, describedValueSetDAOImpl);
            }
        }
    }

    /**
     * Compare Value Set(s) retrieved from imported file to detect whether it is a new one or comparison process is needed
     * for a Multiple Value Sets Response of ITI-60 transaction
     *
     * @param extractedValueSetList Value Set list from imported file
     * @param describedValueSetDAOImpl Implementation of DescribedValueSetDAO
     */
    private void compareValueSetListWithDBForITI60(List<DescribedValueSet> extractedValueSetList, DescribedValueSetDAOImpl describedValueSetDAOImpl) {
        for (DescribedValueSet extractedValueSet : extractedValueSetList) {
            addedConceptList = new ArrayList<>();
            updatedConceptList = new ArrayList<>();
            deletedConceptList = new ArrayList<>();
            retrievedValueSetFromDB = describedValueSetDAOImpl.getValueSetfromDB(extractedValueSet);
            if (retrievedValueSetFromDB == null) {
                if (extractedValueSet.getConceptList() != null && !extractedValueSet.getConceptList().isEmpty()) {
                    setMainListToConceptListAtCreation(extractedValueSet);
                }
                newExtractedValueSetList.add(extractedValueSet);
                addConceptListTypeToExtractedConceptTypeList(extractedValueSet, true);
                if (immediateImportInDB != null && immediateImportInDB) {
                    describedValueSetDAOImpl.addDescribedValueSetFromMultipleValueSetResponse(extractedValueSet);
                }
            } else {
                if (updateRequestedAfterImport != null && updateRequestedAfterImport) {
                    compareConceptListWithDB(extractedValueSet, describedValueSetDAOImpl);
                }
            }
        }
    }

    /**
     * Set mainList attribute of all conceptListTypes of a new imported Value Set
     *
     * @param describedValueSet Value Set from imported file
     */
    private void setMainListToConceptListAtCreation(DescribedValueSet describedValueSet) {
        List<ConceptListType> conceptListTypeList = describedValueSet.getConceptList();
        conceptListTypeList.get(0).setMainList(true);
        for (ConceptListType conceptListType : conceptListTypeList) {
            if (conceptListType.getMainList() == null) {
                conceptListType.setMainList(false);
            }
        }
    }

    /**
     * ConceptLists comparisons, between the one in imported file and in DB, for a given Value Set
     *
     * @param describedValueSet Value Set from imported file
     * @param describedValueSetDAOImpl Implementation of DescribedValueSetDAO
     */
    private void compareConceptListWithDB(DescribedValueSet describedValueSet, DescribedValueSetDAOImpl describedValueSetDAOImpl) {
        CEDAOImpl cedaoImpl = new CEDAOImpl();
        ConceptListTypeDAOImpl conceptListTypeDAOImpl = new ConceptListTypeDAOImpl();
        Boolean modificationInVS = false;
        Boolean modificationInConceptList;
        List<ConceptListType> conceptListTypeList = retrievedValueSetFromDB.getConceptList();
        for (ConceptListType clt : describedValueSet.getConceptList()) {
            Boolean cltExist = false;
            modificationInConceptList = false;
            for (ConceptListType cl : conceptListTypeList) {
                if (clt.getLang().equalsIgnoreCase(cl.getLang())) {
                    cltExist = true;
                    if (deleteRequestedAfterImport != null && deleteRequestedAfterImport && cl.getLang().equals(clt.getLang())) {
                        List<CE> conceptsToRemove = browseConceptAndCompareInConceptList(clt, cl);
                        if (!conceptsToRemove.isEmpty()) {
                            removeConceptFromConceptList(cl, conceptsToRemove, cedaoImpl);
                            modificationInVS = true;
                            modificationInConceptList = true;
                        }
                    }
                    for (CE conceptInImportVS : clt.getConcept()) {
                        Boolean conceptPresentInList = false;
                        for (CE conceptInDBVS : cl.getConcept()) {
                            if (conceptInImportVS.getCode().equals(conceptInDBVS.getCode())) {
                                conceptPresentInList = true;
                                if (modificationInVS) {
                                    modificationInConceptList = checkConceptAttributes(modificationInConceptList, conceptInImportVS, conceptInDBVS);
                                    if (modificationInConceptList) {
                                        cedaoImpl.saveConcept(conceptInDBVS);
                                    }
                                } else {
                                    modificationInVS = checkConceptAttributes(modificationInVS, conceptInImportVS, conceptInDBVS);
                                    if (modificationInVS) {
                                        modificationInConceptList = true;
                                        cedaoImpl.saveConcept(conceptInDBVS);
                                    }
                                }
                            }
                        }
                        if (!conceptPresentInList) {
                            cl.addConcept(conceptInImportVS);
                            addedConceptList.add(conceptInImportVS);
                            FacesMessages.instance().add("Concept : " + conceptInImportVS.getCode() + " added in Concept List : " + cl.getLang());
                            modificationInVS = true;
                            modificationInConceptList = true;
                        }
                        if (clt.getMainList() != null && clt.getMainList() && (clt.getConcept().size() > cl.getConcept().size())) {
                            spreadAddedConceptWhenMainList(clt, conceptInImportVS, cedaoImpl, conceptListTypeDAOImpl);
                            modificationInVS = true;
                            modificationInConceptList = true;
                        }
                    }
                }
            }
            if (!cltExist) {
                addNewConceptList(clt);
                modificationInConceptList = true;
            }
            if (!modificationInConceptList && !upToDateExtractedConceptList.contains(clt)) {
                clt.setValueSet(retrievedValueSetFromDB);
                clt.setMainList(false);
                upToDateExtractedConceptList.add(clt);
            }
        }
        Boolean vsAttributesModified = modifyValueSetAttributes(modificationInVS, describedValueSet);
        if (!vsAttributesModified) {
            upToDateExtractedValueSetList.add(describedValueSet);
        }
        if (modificationInVS) {
            UpdatedDescribedValueSet updatedDescribedValueSet = new UpdatedDescribedValueSet(retrievedValueSetFromDB, addedConceptList, updatedConceptList, deletedConceptList);
            if (!checkIfUpdatedValueSetListContainsValueSet(updatedDescribedValueSet.getDescribedValueSet())) {
                updatedExtractedValueSetList.add(updatedDescribedValueSet);
            }
            if (immediateImportInDB != null && immediateImportInDB) {
                describedValueSetDAOImpl.saveEditValueSet(retrievedValueSetFromDB);
                FacesMessages
                        .instance()
                        .add(retrievedValueSetFromDB.getDisplayName() + " value set and its components have been updated in database");
            }
        } else {
            addConceptListTypeToExtractedConceptTypeList(describedValueSet, false);
        }
    }

    /**
     * Check concept attributes and add to updatedConceptList if needed
     *
     * @param modifications Boolean to be spread across analysis to detect whether a component has been modified during the process or not
     * @param conceptInImportVS Concept to add in other conceptListType of DB
     * @param conceptInDBVS Concept with same code that conceptInImportDBVS
     * @return Boolean If a modification has been done to spread the boolean value in the parent method
     */
    private Boolean checkConceptAttributes(Boolean modifications, CE conceptInImportVS, CE conceptInDBVS) {
        if (!conceptInImportVS.getDisplayName().equals(conceptInDBVS.getDisplayName())) {
            conceptInDBVS.setDisplayName(conceptInImportVS.getDisplayName());
            updatedConceptList.add("Display name updated for concept code " + conceptInImportVS.getCode());
            modifications = true;
        }
        if (!conceptInImportVS.getCodeSystem().equals(conceptInDBVS.getCodeSystem())) {
            conceptInDBVS.setCodeSystem(conceptInImportVS.getCodeSystem());
            updatedConceptList.add("Code system updated for concept code " + conceptInImportVS.getCode());
            modifications = true;
        }
        if (conceptInImportVS.getCodeSystemName() != null) {
            if (!conceptInImportVS.getCodeSystemName().equals(conceptInDBVS.getCodeSystemName())) {
                conceptInDBVS.setCodeSystemName(conceptInImportVS.getCodeSystemName());
                updatedConceptList.add("Code system name updated for concept code " + conceptInImportVS.getCode());
                modifications = true;
            }
        }
        return modifications;
    }

    /**
     * Spread new concept (depending on previous modifications or not) from main conceptListType across all others in a given Value Set
     *
     * @param conceptListTypeInImportVS ConceptListType from imported file
     * @param conceptToAdd Concept to add in other conceptListType of DB
     * @param cedaoImpl Implmementation of CEDAO
     * @param conceptListTypeDAOImpl Implmementation of ConceptListTypeDAO
     */
    private void spreadAddedConceptWhenMainList(ConceptListType conceptListTypeInImportVS, CE conceptToAdd, CEDAOImpl cedaoImpl,
                                                ConceptListTypeDAOImpl conceptListTypeDAOImpl) {
        for (ConceptListType conceptListType : retrievedValueSetFromDB.getConceptList()) {
            if (conceptListTypeInImportVS.getConcept().size() > conceptListType.getConcept().size()) {
                if (!checkIfConceptListContainsConceptCode(conceptListType, conceptToAdd.getCode())) {
                    conceptListType.addConcept(conceptToAdd);
                    addedConceptList.add(conceptToAdd);
                    if (immediateImportInDB != null && immediateImportInDB) {
                        conceptListType = cedaoImpl.addConceptToConceptList(conceptListType, conceptToAdd);
                        conceptListTypeDAOImpl.saveConceptList(retrievedValueSetFromDB, conceptListType);
                    }
                }
            }
        }
    }

    /**
     * Add new conceptList to Value Set in DB (new ConceptListType in imported Value Set)
     *
     * @param clt ConceptListType to add in DB
     */
    private void addNewConceptList(ConceptListType clt) {
        newExtractedConceptTypeList.add(clt);
        clt.setValueSet(retrievedValueSetFromDB);
        clt.setMainList(false);
        for (CE c : clt.getConcept()) {
            c.setConceptList(clt);
        }
        retrievedValueSetFromDB.getConceptList().add(clt);
    }

    /**
     * Browse concepts from imported conceptList and compare them to the same one in DB
     *
     * @param conceptListTypefromImport ConceptListType from imported file
     * @param conceptListTypeFromDB ConceptListType from DB
     * @return List of concept(s) to remove
     */
    private List<CE> browseConceptAndCompareInConceptList(ConceptListType conceptListTypefromImport, ConceptListType conceptListTypeFromDB) {
        List<CE> conceptToRemoveList = new ArrayList<>();
        for (CE conceptDB : conceptListTypeFromDB.getConcept()) {
            Boolean conceptInList = false;
            for (CE conceptImport : conceptListTypefromImport.getConcept()) {
                if (conceptDB.getCode().equals(conceptImport.getCode())) {
                    conceptInList = true;
                }
            }
            if (!conceptInList) {
                deletedConceptList.add(conceptDB);
                conceptToRemoveList.add(conceptDB);
            }
        }
        return conceptToRemoveList;
    }

    /**
     * Remove concept from conceptListType and persist in DB
     *
     * @param conceptListTypeDB Concept List Type in DB
     * @param conceptsToRemove List of CE to remove from conceptListTypeDB concepts
     * @param cedaoImpl Implmementation of CEDAO
     */
    private void removeConceptFromConceptList(ConceptListType conceptListTypeDB, List<CE> conceptsToRemove, CEDAOImpl cedaoImpl) {
        for (CE concept : conceptsToRemove) {
            if (immediateImportInDB != null && immediateImportInDB) {
                conceptListTypeDB = cedaoImpl.deleteConceptFromConceptList(conceptListTypeDB, concept);
            }
        }
    }

    /**
     * Modify Value Set attributes, by comparison with imported one (with same id)
     *
     * @param modifications Boolean to be spread across analysis to detect whether a component has been modified during the process or not
     * @param describedValueSet Imported Value Set
     * @return Boolean (modification or not during comparison
     */
    private Boolean modifyValueSetAttributes(Boolean modifications, DescribedValueSet describedValueSet) {
        if (describedValueSet.getBinding() != null && !describedValueSet.getBinding().equals(retrievedValueSetFromDB.getBinding())) {
            retrievedValueSetFromDB.setBinding(describedValueSet.getBinding());
            modifications = true;
        }
        if (describedValueSet.getDefinition() != null && !describedValueSet.getDefinition().equals(retrievedValueSetFromDB.getDefinition())) {
            retrievedValueSetFromDB.setDefinition(describedValueSet.getDefinition());
            modifications = true;
        }
        if (describedValueSet.getDisplayName() != null && !describedValueSet.getDisplayName().equals(retrievedValueSetFromDB.getDisplayName())) {
            retrievedValueSetFromDB.setDisplayName(describedValueSet.getDisplayName());
            modifications = true;
        }
        if (describedValueSet.getGroup() != null && !describedValueSet.getGroup().equals(retrievedValueSetFromDB.getGroup())) {
            retrievedValueSetFromDB.setGroup(describedValueSet.getGroup());
            modifications = true;
        }
        if (describedValueSet.getPurpose() != null && !describedValueSet.getPurpose().equals(retrievedValueSetFromDB.getPurpose())) {
            retrievedValueSetFromDB.setPurpose(describedValueSet.getPurpose());
            modifications = true;
        }
        if (describedValueSet.getSource() != null && !describedValueSet.getSource().equals(retrievedValueSetFromDB.getSource())) {
            retrievedValueSetFromDB.setSource(describedValueSet.getSource());
            modifications = true;
        }
        if (describedValueSet.getVersion() != null && !describedValueSet.getVersion().equals(retrievedValueSetFromDB.getVersion())) {
            retrievedValueSetFromDB.setVersion(describedValueSet.getVersion());
            modifications = true;
        }
        if (describedValueSet.getCreationDate() != null && !describedValueSet.getCreationDate().equals(retrievedValueSetFromDB.getCreationDate())) {
            retrievedValueSetFromDB.setCreationDate(describedValueSet.getCreationDate());
            modifications = true;
        }
        if (describedValueSet.getEffectiveDate() != null && !describedValueSet.getEffectiveDate().equals(retrievedValueSetFromDB.getEffectiveDate())) {
            retrievedValueSetFromDB.setEffectiveDate(describedValueSet.getEffectiveDate());
            modifications = true;
        }
        if (describedValueSet.getExpirationDate() != null && !describedValueSet.getExpirationDate().equals(retrievedValueSetFromDB.getExpirationDate())) {
            retrievedValueSetFromDB.setExpirationDate(describedValueSet.getExpirationDate());
            modifications = true;
        }
        if (describedValueSet.getRevisionDate() != null && !describedValueSet.getRevisionDate().equals(retrievedValueSetFromDB.getRevisionDate())) {
            retrievedValueSetFromDB.setRevisionDate(describedValueSet.getRevisionDate());
            modifications = true;
        }
        return modifications;
    }

    /**
     * Add conceptListType to extractedConceptTypeList depending on if it is a new one or an up to date one
     *
     * @param describedValueSet Imported Value Set
     * @param isNew Boolean to indicate whether we are in an addition process or an up up-to-date detection one
     */
    private void addConceptListTypeToExtractedConceptTypeList(DescribedValueSet describedValueSet, Boolean isNew) {
        for (ConceptListType conceptListType : describedValueSet.getConceptList()) {
            if (conceptListType.getValueSet() == null) {
                conceptListType.setValueSet(describedValueSet);
            }
            if (conceptListType.getMainList() == null) {
                conceptListType.setMainList(false);
            }
            if (isNew) {
                newExtractedConceptTypeList.add(conceptListType);
            } else {
                if (!newExtractedConceptTypeList.contains(conceptListType) && !upToDateExtractedConceptList.contains(conceptListType)) {
                    upToDateExtractedConceptList.add(conceptListType);
                }
            }
        }
    }

    /**
     * Download value sets from list
     *
     * @param vss List of value sets
     */
    public void downloadValueSets(List<DescribedValueSet> vss) {
        try {
            Path tempDir = Files.createTempDirectory("ValueSets");
            for (DescribedValueSet describedValueSet : vss) {
                FacesContext fc = FacesContext.getCurrentInstance();
                HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
                URI contextUrl = URI.create(req.getRequestURL().toString()).resolve(req.getContextPath());

                writeInFile(contextUrl.toString() + "/rest/RetrieveValueSetForSimulator?id=", describedValueSet.getId(), tempDir);
            }
            String archive_file = "/tmp/archive.zip";
            File archfile = new File(archive_file);
            ZipUtility.zipDirectory(tempDir.toFile(), archfile);

            DocumentFileUpload.showFile(archive_file, "archive.zip", true);

            ZipUtility.deleteDirectory(tempDir.toFile());


            if (archfile.delete()) {
                LOG.info("archfile deleted");
            } else {
                LOG.error("Failed to delete archfile");
            }
        } catch (IOException e) {
            LOG.error("IOException : " + e.getMessage());
        }
    }

    /**
     * Create a temp dir for store temp xml files
     *
     * @return Return the directory created
     */
    public File createTempDir() {
        // Get the temp directory
        File baseDir = new File(System.getProperty("java.io.tmpdir"));
        String baseName = System.currentTimeMillis() + "SVS";

        // Create the temp directory, used time for the name
        for (int counter = 0; counter < 100; counter++) {
            File tempDir = new File(baseDir, baseName + counter);
            if (tempDir.mkdir()) {
                return tempDir;
            }
        }
        throw new IllegalStateException("Failed to create directory within " + 100 + " attempts (tried " + baseName
                + "0 to " + baseName + (100 - 1) + ')');
    }

    /**
     * Return witch type of xml file has been send
     *
     * @param fileContent Content of the file to test
     * @return string
     */
    public String returnXmlType(String fileContent) {

        // Looking for a occurence of any type of SVS Request and return if the type is found
        if (fileContent.indexOf("RetrieveValueSetResponse") > 0) {
            return "RetrieveValueSetResponse";
        } else if (fileContent.indexOf("RetrieveValueSetRequest") > 0) {
            return "RetrieveValueSetRequest";
        } else if (fileContent.indexOf("RetrieveMultipleValueSetsResponse") > 0) {
            return "RetrieveMultipleValueSetsResponse";
        } else if (fileContent.indexOf("RetrieveMultipleValueSetsRequest") > 0) {
            return "RetrieveMultipleValueSetsRequest";
        } else {
            return "";
        }
    }

    /**
     * Unmarshall a document
     *
     * @param clazz       The class to the object (xml to Class)
     * @param inputStream The xml content
     * @param <T>         type
     * @return The object get from the xml file
     * @throws JAXBException jaxbe
     */
    @SuppressWarnings("unchecked")
    public static <T> T unmarshallDocument(Class<T> clazz, InputStream inputStream) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance("net.ihe.gazelle.svs");
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (T) unmarshaller.unmarshal(inputStream);
    }


    /**
     * Write in a file in tempDir directory using a stream
     *
     * @param urlToReach  URL to open stream
     * @param vsId   Id of the valueSet to be added to file name
     * @param tempDir   Path of temporary directory
     */
    public void writeInFile(String urlToReach, String vsId, Path tempDir) {
        URL url;
        InputStream is = null;
        String line;
        try {
            File tmpFile = new File(tempDir.toFile() + "/" + vsId + ".xml");
            Path dst = Paths.get(tmpFile.getPath());
            //LOG.info("file path : " + dst.toString());

            url = new URL(urlToReach + vsId);
            is = url.openStream();  // throws an IOException
            try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
                // write the inputStream to a FileOutputStream
                try (BufferedWriter writer = Files.newBufferedWriter(dst, StandardCharsets.UTF_8)) {

                    while ((line = br.readLine()) != null) {
                        writer.write(line);
                        // must do this: .readLine() will have stripped line endings
                        writer.newLine();
                    }
                    if (writer != null) {
                        writer.close();
                    }
                }
            }
        } catch (MalformedURLException mue) {
            LOG.error("MalformedURLException : " + mue.getMessage());
        } catch (IOException ioe) {
            LOG.error("IOException : " + ioe.getMessage());
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException ioe) {
            }
        }
    }


    /**
     * Check if UpdatedExtractedValueSetList already contains a certain valueSet
     *
     * @param describedValueSet Value Set to use in comparison process
     * @return Boolean
     */
    private Boolean checkIfUpdatedValueSetListContainsValueSet(DescribedValueSet describedValueSet) {
        for (UpdatedDescribedValueSet updatedDescribedValueSet : updatedExtractedValueSetList) {
            if (updatedDescribedValueSet.getDescribedValueSet().getIdDB().equals(describedValueSet.getIdDB())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a specific conceptListType already contains a concept with a certain code or not
     *
     * @param conceptListType ConceptListType to use in comparison process
     * @param code Code to use in comparison process
     * @return Boolean
     */
    private Boolean checkIfConceptListContainsConceptCode(ConceptListType conceptListType, String code) {
        for (CE concept : conceptListType.getConcept()) {
            if (concept.getCode().equals(code)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Reset all global lists of the class
     */
    public void resetLists() {
        setNewExtractedValueSetList(new ArrayList<DescribedValueSet>());
        setNewExtractedConceptTypeList(new ArrayList<ConceptListType>());
        setUpdatedExtractedValueSetList(new ArrayList<UpdatedDescribedValueSet>());
        setAddedConceptList(new ArrayList<CE>());
        setUpdatedConceptList(new ArrayList<String>());
        setDeletedConceptList(new ArrayList<CE>());
        setUpToDateExtractedValueSetList(new ArrayList<DescribedValueSet>());
        setUpToDateExtractedConceptList(new ArrayList<ConceptListType>());
    }

    public Boolean hasChangesMade() {
        if (!newExtractedValueSetList.isEmpty() || !newExtractedConceptTypeList.isEmpty() || !updatedExtractedValueSetList.isEmpty())  {
            return true;
        } else {
            return false;
        }
    }
}
