package net.ihe.gazelle.simulator.svs.business;

import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.DescribedValueSet;

import javax.persistence.Tuple;
import java.util.ArrayList;
import java.util.List;

/**
 * UpdatedDescribedValueSet is an object linking a DescribedValueSet which has been updated by import and lists of Concepts depending on their states
 * (added/updated/described)
 *
 * @author nbailliet
 */

public class UpdatedDescribedValueSet {

    DescribedValueSet describedValueSet;

    List<CE> addedConceptList;

    List<String> updatedConceptList;

    List<CE> deletedConceptList;

    public DescribedValueSet getDescribedValueSet() {
        return describedValueSet;
    }

    public void setDescribedValueSet(DescribedValueSet describedValueSet) {
        this.describedValueSet = describedValueSet;
    }

    public List<CE> getAddedConceptList() {
        return addedConceptList;
    }

    public void setAddedConceptList(List<CE> addedConceptList) {
        this.addedConceptList = addedConceptList;
    }

    public void addAddedConceptToList(CE concept) {
        if (addedConceptList != null && concept != null) {
            addedConceptList.add(concept);
        }
    }

    public List<String> getUpdatedConceptList() {
        return updatedConceptList;
    }

    public void setUpdatedConceptList(List<String> updatedConceptList) {
        this.updatedConceptList = updatedConceptList;
    }

    public void addUpdatedConceptToList(String concept) {
        if (updatedConceptList != null && concept != null) {
            updatedConceptList.add(concept);
        }
    }

    public List<CE> getDeletedConceptList() {
        return deletedConceptList;
    }

    public void setDeletedConceptList(List<CE> deletedConceptList) {
        this.deletedConceptList = deletedConceptList;
    }

    public void addDeletedConceptToList(CE concept) {
        if (deletedConceptList != null && concept != null) {
            deletedConceptList.add(concept);
        }
    }

    public UpdatedDescribedValueSet(DescribedValueSet valueSet) {
        this.describedValueSet = valueSet;
        this.addedConceptList = new ArrayList<>();
        this.updatedConceptList = new ArrayList<>();
        this.deletedConceptList = new ArrayList<>();
    }

    public UpdatedDescribedValueSet(DescribedValueSet valueSet, List<CE> addedConceptList, List<String> updatedConceptList, List<CE> deletedConceptList) {
        this.describedValueSet = valueSet;
        this.addedConceptList = addedConceptList;
        this.updatedConceptList = updatedConceptList;
        this.deletedConceptList = deletedConceptList;
    }
}
