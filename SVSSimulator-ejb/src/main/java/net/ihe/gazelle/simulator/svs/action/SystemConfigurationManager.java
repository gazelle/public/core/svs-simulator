package net.ihe.gazelle.simulator.svs.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.action.ApplicationConfigurationManager;
import net.ihe.gazelle.simulator.common.action.ApplicationConfigurationManagerLocal;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;
import net.ihe.gazelle.simulator.svs.filter.RepositorySystemConfigurationFilter;
import net.ihe.gazelle.simulator.svs.model.RepositorySystemConfiguration;
import net.ihe.gazelle.simulator.svs.model.RepositorySystemConfigurationQuery;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.security.Identity;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <b>System Configuration Manager is the class which contains all operations of the SUT Configuration from SVS Simulator Application.</b>
 * 
 * @author mtual
 * @version 1.0
 */

@Name("systemConfigurationManager")
@Synchronized(timeout = 350000)
@Scope(ScopeType.PAGE)
public class SystemConfigurationManager extends AbstractSystemConfigurationManager implements Serializable, UserAttributeCommon {

	private static final long serialVersionUID = 1L;
	public static final String OWNER = "owner";

	// ------------------- //
	// ---- Variables ---- //
	// ------------------- //

	private RepositorySystemConfiguration sutExtract;
	private transient Filter<RepositorySystemConfiguration> filter;

	@In(value="gumUserService")
	private UserService userService;

	/**
	 * Get the list of Binding Type for a selectListItem
	 * 
	 * @return List Items for Select
	 */
	public List<SelectItem> getAvailableBindingsTypeAsItem() {
		// Load from the entity class SystemConfiguration
		RepositorySystemConfiguration.BindingType[] listBT = RepositorySystemConfiguration.BindingType.values();
		List<String> strings = new ArrayList<String>();
		for (RepositorySystemConfiguration.BindingType bt : listBT) {
			strings.add(bt.getValue());
		}
		List<SelectItem> items = new ArrayList<SelectItem>(strings.size());
		for (String value : strings) {
			items.add(new SelectItem(value));
		}
		return items;
	}


	/**
	 * Deactivate a System Configuration
     *
     * @param desactivateSUT The SystemConfiguration to deactivate
     */
    public void desactivateSystemConfiguration(RepositorySystemConfiguration desactivateSUT) {
		// If exist we deactivate him
		if (desactivateSUT != null) {
			desactivateSUT.setIsAvailable(false);
			desactivateSUT.setLastChanged(new Date());

			EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
			entityManager.merge(desactivateSUT);
			entityManager.flush();

			FacesMessages.instance().add("The System Configuration has been desactivated.");
		}
	}

	/**
	 * Activate a System Configuration
     *
     * @param activateSUT The SystemConfiguration to activate
     */
    public void activateSystemConfiguration(RepositorySystemConfiguration activateSUT) {
		// If exist we activate him
		if (activateSUT != null) {
			activateSUT.setIsAvailable(true);
			activateSUT.setLastChanged(new Date());

			EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
			entityManager.merge(activateSUT);
			entityManager.flush();

			FacesMessages.instance().add("The System Configuration has been activated.");
		}
	}


	/**
	 * Allow to secure the access to SUT
     *
     * @param sut The SystemConfiguration
     * @return Return a Boolean (true : can display) (false : not accorded)
     */
    public Boolean secureViewSUT(RepositorySystemConfiguration sut) {
		// If new sut display = true
		if (sut.getId() == null) {
			return true;
		}

		// Get identity of user and sut from database
		Identity identity = Identity.instance();
		HQLQueryBuilder<RepositorySystemConfiguration> queryBuilder = new HQLQueryBuilder<RepositorySystemConfiguration>(
				RepositorySystemConfiguration.class);
		queryBuilder.addEq("id", sut.getId());
		RepositorySystemConfiguration sutDB = queryBuilder.getUniqueResult();

		ApplicationConfigurationManagerLocal manager = (ApplicationConfigurationManagerLocal) Component
				.getInstance("applicationConfigurationManager");
		// Test each case and return true or false
		if (manager.isUserAllowedAsAdmin()) {
			return true;
		} else if (!sutDB.getIsAvailable()) {
			return false;
		} else if (!sutDB.getIsPublic()) {
			return identity.getCredentials().getUsername().equalsIgnoreCase(sutDB.getOwner());
		} else {
			return true;
		}
	}


	public RepositorySystemConfiguration getSutExtract() {
		if (this.sutExtract == null) {
			this.sutExtract = new RepositorySystemConfiguration();
		}
		return sutExtract;
	}

	public void setSutExtract(RepositorySystemConfiguration sutExtract) {
		this.sutExtract = sutExtract;
	}

	public Filter<RepositorySystemConfiguration> getFilter() {
		if (filter == null) {
			filter = new Filter<RepositorySystemConfiguration>(getHQLCriteriaForFilter());
			this.filter.getFormatters().put(OWNER, new UserValueFormatter(this.filter, OWNER));
		}
		return filter;
	}

    public void setFilter(RepositorySystemConfigurationFilter filter) {
        this.filter = filter;
    }

	@Override
	protected HQLCriterionsForFilter getHQLCriterionsForFilter() {
		return null;
	}

	public FilterDataModel<RepositorySystemConfiguration> getSystemConfiguration() {
		return new FilterDataModel<RepositorySystemConfiguration>(getFilter()) {
			@Override
			protected Object getId(RepositorySystemConfiguration sytemConfiguration) {
				return sytemConfiguration.getId();
			}
		};
	}

	private HQLCriterionsForFilter<RepositorySystemConfiguration> getHQLCriteriaForFilter() {
		RepositorySystemConfigurationQuery query = new RepositorySystemConfigurationQuery();
		HQLCriterionsForFilter<RepositorySystemConfiguration> criteria = query.getHQLCriterionsForFilter();
		criteria.addPath("name", query.name());
		criteria.addPath(OWNER, query.owner());
		criteria.addPath("bindingType", query.bindingType());
		criteria.addPath("url", query.url());
		criteria.addPath("isAvailable", query.isAvailable());
		criteria.addQueryModifier(this);
		return criteria;
	}

	@Override
	public void modifyQuery(HQLQueryBuilder hqlQueryBuilder, Map map) {

		if(this.sutExtract != null){
			if (this.sutExtract.getName() != null && !this.sutExtract.getName().isEmpty()) {
				hqlQueryBuilder.addRestriction(HQLRestrictions.like("name", sutExtract.getName(),
						HQLRestrictionLikeMatchMode.ANYWHERE));
			}

			if (this.sutExtract.getOwner() != null && !this.sutExtract.getOwner().isEmpty()) {
				hqlQueryBuilder.addRestriction(HQLRestrictions.like("username", sutExtract.getOwner(),
						HQLRestrictionLikeMatchMode.ANYWHERE));
			}

			if (this.sutExtract.getEndpoint() != null && !this.sutExtract.getEndpoint().isEmpty()) {
				hqlQueryBuilder.addRestriction(HQLRestrictions.like("endpoint", sutExtract.getEndpoint(),
						HQLRestrictionLikeMatchMode.ANYWHERE));
			}

			if (this.sutExtract.getBindingType() != null && !this.sutExtract.getBindingType().isEmpty()) {
				hqlQueryBuilder.addRestriction(HQLRestrictions.like("bindingType", sutExtract.getBindingType(),
						HQLRestrictionLikeMatchMode.ANYWHERE));
			}
		}

		if (!Identity.instance().isLoggedIn()) {
			hqlQueryBuilder.addRestriction(HQLRestrictions.eq("isAvailable", true));
			hqlQueryBuilder.addRestriction(HQLRestrictions.eq("isPublic", true));
		} else if (!ApplicationConfigurationManager.instance().isUserAllowedAsAdmin()) {
			if (this.sutExtract != null && this.sutExtract.getIsAvailable() != null) {
				hqlQueryBuilder.addRestriction(HQLRestrictions.eq("isAvailable", sutExtract.getIsAvailable()));
			}
		} else {
			hqlQueryBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
					HQLRestrictions.eq(OWNER, Identity.instance().getCredentials().getUsername())));
		}
	}

	public void reset() {
		getFilter().clear();
	}

	@Override
	public void copySelectedConfiguration(SystemConfiguration inConfiguration) {
		if (inConfiguration != null) {
			selectedSystemConfiguration = new RepositorySystemConfiguration(inConfiguration);
			editSelectedConfiguration();
		} else {
			FacesMessages.instance().add("No configuration selected");
		}
	}

	@Override
	public void addConfiguration() {
		selectedSystemConfiguration = new RepositorySystemConfiguration();
		displayEditPanel = true;
		displayListPanel = false;
	}

	@Override
	public void deleteSelectedSystemConfiguration() {
		if (selectedSystemConfiguration != null) {
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			RepositorySystemConfiguration configToRemove = entityManager.find(
					RepositorySystemConfiguration.class, selectedSystemConfiguration.getId());
			entityManager.remove(configToRemove);
			entityManager.flush();
			selectedSystemConfiguration = null;
			reset();
			FacesMessages.instance().add("SUT configuration successfully deleted");
		} else {
			FacesMessages.instance().add("No SUT selected for deletion");
		}
	}

	@Override
	public List<Usage> getPossibleListUsages() {
		UsageQuery query = new UsageQuery();
		return query.getList();
	}

	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}
}
