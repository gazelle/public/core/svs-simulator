package net.ihe.gazelle.simulator.svs.dao;

import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.svs.ConceptListType;

public interface CEDAO {

    CE getConceptFromCode(String code);
    void saveConcept(CE concept);
    ConceptListType addConceptToConceptList(ConceptListType conceptListType, CE concept);
    ConceptListType deleteConceptFromConceptList(ConceptListType conceptListType, CE concept);
}
