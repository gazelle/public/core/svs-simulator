package net.ihe.gazelle.simulator.svs.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.svs.business.UpdatedDescribedValueSet;
import net.ihe.gazelle.simulator.svs.business.ValueSetExtractor;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.DescribedValueSet;
import net.ihe.svs.GazelleSVSValidator;
import org.apache.james.mime4j.util.CharsetUtil;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesManager;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.*;

/**
 * <b>ImportFromFileManager is the class which contains all operations of the import from a file functionality of SVSimulator.</b>
 *
 * @author nbailliet
 * @version 1.0
 */

@Name("importFromFileManager")
@Synchronized(timeout = 350000)
@Scope(ScopeType.PAGE)
public class ImportFromFileManager implements Serializable {

    private static Logger LOG = LoggerFactory.getLogger(ValueSetExtractor.class);

    private ValueSetExtractor valueSetExtractor = new ValueSetExtractor();

    private static FilenameFilter xmlFilter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.endsWith(".xml");
        }
    };

    private Boolean updateRequested = false;
    private Boolean deleteRequested = false;
    private Boolean reviewChanges = true;

    private String pathToFolderToImport = "";

    private Map<String, byte[]> filesContent = new HashMap<String, byte[]>();
    private Map<String, byte[]> zipFile = new HashMap<String, byte[]>();

    public Boolean getUpdateRequested() {
        return updateRequested;
    }

    public void setUpdateRequested(Boolean updateRequested) {
        this.updateRequested = updateRequested;
    }

    public Boolean getDeleteRequested() {
        return deleteRequested;
    }

    public void setDeleteRequested(Boolean deleteRequested) {
        this.deleteRequested = deleteRequested;
    }

    public Boolean getReviewChanges() {
        return reviewChanges;
    }

    public void setReviewChanges(Boolean reviewChanges) {
        this.reviewChanges = reviewChanges;
    }

    public String getPathToFolderToImport() {
        return pathToFolderToImport;
    }

    public void setPathToFolderToImport(String pathToFolderToImport) {
        this.pathToFolderToImport = pathToFolderToImport;
    }

    public Map<String, byte[]> getFilesContent() {
        return filesContent;
    }

    public void setFilesContent(Map<String, byte[]> filesContent) {
        this.filesContent = filesContent;
    }

    public Map<String, byte[]> getZipFile() {
        return zipFile;
    }

    public void setZipFile(Map<String, byte[]> zipFile) {
        this.zipFile = zipFile;
    }

    public List<DescribedValueSet> getNewImportedValueSetList() {
        return valueSetExtractor.getNewExtractedValueSetList();
    }

    public List<ConceptListType> getNewImportedConceptTypeList() {
        return valueSetExtractor.getNewExtractedConceptTypeList();
    }

    public List<UpdatedDescribedValueSet> getUpdatedImportedValueSetList() {
        return valueSetExtractor.getUpdatedExtractedValueSetList();
    }

    public List<DescribedValueSet> getUpToDateImportedValueSetList() {
        return valueSetExtractor.getUpToDateExtractedValueSetList();
    }
    public List<ConceptListType> getUpToDateImportedConceptList() {
        return valueSetExtractor.getUpToDateExtractedConceptList();
    }

    public Boolean hasChangesMade() {
        return valueSetExtractor.hasChangesMade();
    }

    public List<UpdatedDescribedValueSet> createListFromUpdatedDescribedValueSet(UpdatedDescribedValueSet updatedDescribedValueSet) {
        List<UpdatedDescribedValueSet> updatedDescribedValueSetList = new ArrayList<>();
        if (updatedDescribedValueSet != null) {
            updatedDescribedValueSetList.add(updatedDescribedValueSet);
        }
        return updatedDescribedValueSetList;
    }

    /**
     * Listener for uploader. Get the upload items and call the method to change this uploaded items in ValueSet.
     *
     * @param event The occured event
     * @throws Exception e
     */
    public void listener(FileUploadEvent event) throws Exception {
        clearUploadData(); // Clear the previously upload data
        UploadedFile item = event.getUploadedFile(); // Get the new uploaded item

        String fileName = item.getName();
        String extension = fileName.substring(fileName.lastIndexOf('.') + 1);

        // If it's a zip file, call the method zipToObject
        if (extension.equalsIgnoreCase("zip")) {

            if (zipFile == null || zipFile.size() == 0) {
                zipFile = new HashMap<>();
                zipFile.put(item.getName(), item.getData());
            }
            if (reviewChanges) {
                valueSetExtractor.zipToObject(updateRequested, deleteRequested, zipFile, filesContent, false);
            } else {
                valueSetExtractor.zipToObject(updateRequested, deleteRequested, zipFile, filesContent, true);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Changes imported in database");
            }
        } else if (extension.equalsIgnoreCase("xml")) // If it's xml file call directly xmlToObject
        {
            if (filesContent == null) {
                filesContent = new HashMap<String, byte[]>();
            }
            filesContent.put(item.getName(), item.getData());
            if (reviewChanges) {
                valueSetExtractor.xmlToObject(updateRequested, deleteRequested, filesContent, false);
            } else {
                valueSetExtractor.xmlToObject(updateRequested, deleteRequested, filesContent, true);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Changes imported in database");
            }
        } else // Error case
        {
            FacesMessages.instance().add(
                    "The extension: " + extension + " is not supported. Please use xml or zip files.");
            return;
        }
    }

    public String back() {
        return "/browser/importValueSetFromFile.seam";
    }

    public void importToRepository() {
        //Persist data in DB
        try {
            valueSetExtractor.xmlToObject(updateRequested, deleteRequested, filesContent, true);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Changes imported in database");
        } catch (Exception e) {
            e.printStackTrace();
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to persist data in database " + e.getMessage());
        }
    }

    /**
     * Allow to import xml file from specific folder in the server
     */
    public void importFromFolder() {
        if (pathToFolderToImport != null && !pathToFolderToImport.isEmpty()) {
            File baseDirectory = new File(pathToFolderToImport);
            if (baseDirectory.exists() && baseDirectory.isDirectory()) {
                filesContent = new HashMap<String, byte[]>();
                String[] listFilesNames = baseDirectory.list(xmlFilter);
                if (listFilesNames != null) {
                    for (String fileName : listFilesNames) {
                        File svsFile = new File(baseDirectory, fileName);
                        FileInputStream fis = null;
                        try {
                            fis = new FileInputStream(svsFile);
                            byte[] buffer = new byte[(int) svsFile.length()];
                            fis.read(buffer);
                            filesContent.put(fileName, buffer);
                        } catch (IOException e) {
                            FacesMessages.instance().add("Unable to read content from file " + fileName);
                        } finally {
                            if (fis != null) {
                                try {
                                    fis.close();
                                } catch (IOException e) {
                                    LOG.error("Application has been unable to close file input stream for " + fileName);
                                }
                            }
                        }
                    }
                }

                valueSetExtractor.xmlToObject(updateRequested, deleteRequested, filesContent, true);
                filesContent = null;
            } else {
                FacesMessages.instance().add(pathToFolderToImport + " does not exist on server or is not a directory");
            }
        } else {
            FacesMessages.instance().add("The path to access the files to import is null or empty");
        }
    }

    /**
     * Get numbers of uploaded files
     *
     * @return Return the numbers of uploaded files
     */
    public int getSize() {
        // Return size of the 2 var use to contained files uploaded
        if (getFilesContent() != null) {
            if (getFilesContent().size() > 0) {
                return getFilesContent().size();
            }
        }
        if (getZipFile() != null) {
            if (getZipFile().size() > 0) {
                return getZipFile().size();
            }
        }

        return 0;
    }

    /**
     * Clear the uploaded files
     */
    public void clearUploadData() {
        if (filesContent != null) {
            filesContent.clear();
        }
        if (zipFile != null) {
            zipFile.clear();
        }
        valueSetExtractor.resetLists();
    }

    /**
     * Get the list of download files
     *
     * @return The list of name, download files
     */
    public List<String> getFilesContentkeySet() {

        List<String> listKeys = new ArrayList<String>();
        if (this.filesContent != null) {
            Set<String> keys = this.filesContent.keySet();
            for (String str : keys) {
                listKeys.add(str);
            }
        }
        return listKeys;
    }

    /**
     * Get the KeySet of zipFile for display name of uploaded files to user
     *
     * @return List of KeySet
     */
    public List<String> getZipFilekeySet() {
        Set<String> keys = this.zipFile.keySet();
        List<String> listKeys = new ArrayList<String>();
        for (String str : keys) {
            listKeys.add(str);
        }
        return listKeys;
    }

    private void detectIfEverythingIsUpToDate() {
        if (getNewImportedValueSetList().isEmpty() && getUpToDateImportedValueSetList().isEmpty() && !getUpToDateImportedValueSetList().isEmpty()) {
            FacesMessages.instance().add("All the value sets of the file are ûp-to-date, nothing to modify in repository");
        }
    }
}
