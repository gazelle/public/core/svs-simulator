package net.ihe.gazelle.simulator.svs.dao;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.DescribedValueSet;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.persistence.EntityManager;

@Name("conceptListTypeDAO")
public class ConceptListTypeDAOImpl implements ConceptListTypeDAO {

    private EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");

    /**
     * Delete a concept list from is database id
     * @param valueSet Value set to be refreshed after deleting the conceptList
     * @param idDB The id of the ConceptList in database
     */
    public void deleteConceptList(DescribedValueSet valueSet, Integer idDB) {
        HQLQueryBuilder<ConceptListType> queryBuilder = new HQLQueryBuilder<ConceptListType>(ConceptListType.class);
        queryBuilder.addEq("idDB", idDB);
        ConceptListType conceptListToDelete = queryBuilder.getUniqueResult();

        // We can't delete a main conceptList
        if (!conceptListToDelete.getMainList()) {
            entityManager.remove(conceptListToDelete);
            entityManager.flush();

            // Refresh the selectedValueSet after the modification of his conceptList
            HQLQueryBuilder<DescribedValueSet> queryBuilderRefresh = new HQLQueryBuilder<DescribedValueSet>(
                    DescribedValueSet.class);
            queryBuilderRefresh.addEq("idDB", valueSet.getIdDB());
            valueSet = queryBuilderRefresh.getUniqueResult();
        } else {
            FacesMessages
                    .instance()
                    .add("You can't delete the main Concept List of the Value Set. Edit the main conceptList for make any changes.");
        }
    }

    /**
     * Save a conceptList
     * @param valueSet Value set to be used to retrieve its conceptList
     * @param conceptList The ConceptList to save
     */
    public void saveConceptList(DescribedValueSet valueSet, ConceptListType conceptList) {
        // Test Exist + create new name if exist
        Integer count = 1;
        Boolean exist;
        String saveLang = conceptList.getLang();
        do {
            exist = false;
            for (ConceptListType clt : valueSet.getConceptList()) {
                if (clt.getLang() == null || clt.getLang().isEmpty()) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Lang can't be empty");
                    return;
                } else if (clt.getLang().equalsIgnoreCase(conceptList.getLang()) && !clt.getIdDB().equals(conceptList.getIdDB())) {
                    exist = true;
                    count++;
                    conceptList.setLang(saveLang + "." + count);
                }
            }
        } while (exist);

        entityManager.merge(conceptList);
        entityManager.flush();
    }
}
