package net.ihe.gazelle.simulator.svs.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.svs.GroupType;

public class GroupDataModel extends FilterDataModel<GroupType> {

	private GroupType groupExtract;

	public GroupDataModel(Filter<GroupType> filter, GroupType group) {
		super(filter);
		this.groupExtract = group;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<GroupType> queryBuilder) {

		if (this.groupExtract.getID() != null && !this.groupExtract.getID().isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("iD", groupExtract.getID(),
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}

		if (this.groupExtract.getDisplayName() != null && !this.groupExtract.getDisplayName().isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("displayName", groupExtract.getDisplayName(),
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}

		if (this.groupExtract.getSourceOrganization() != null && !this.groupExtract.getSourceOrganization().isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("sourceOrganization",
					groupExtract.getSourceOrganization(), HQLRestrictionLikeMatchMode.ANYWHERE));
		}
	}

	public GroupType getSutExtract() {
		if (groupExtract == null) {
			groupExtract = new GroupType();
		}
		return groupExtract;
	}

	public void setVsExtract(GroupType groupExtract) {
		this.groupExtract = groupExtract;
	}

@Override
        protected Object getId(GroupType t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
