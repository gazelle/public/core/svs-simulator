package net.ihe.gazelle.simulator.svs.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.svs.DescribedValueSet;
import net.ihe.gazelle.svs.GroupType;
import net.ihe.gazelle.svs.GroupTypeQuery;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("groupTypeDAO")
public class GroupTypeDAOImpl implements GroupTypeDAO {

    private EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");

    private transient Filter<GroupType> filterGroup;

    public Filter<GroupType> getFilterGroup() {
        if (filterGroup == null) {
            filterGroup = new Filter<GroupType>(getHQLCriteriaForFilterGroup());
        }
        return filterGroup;
    }

    public void setFilterGroup(Filter<GroupType> filterGroup) {
        this.filterGroup = filterGroup;
    }

    public FilterDataModel<GroupType> getAllGroups() {
        return new FilterDataModel<GroupType>(getFilterGroup()) {
            @Override
            protected Object getId(GroupType groupType) {
                return groupType.getId();
            }
        };
    }

    public GroupType getGroupTypeFromUrl(Map<String,String> urlParams) {
        HQLQueryBuilder<GroupType> queryBuilder = new HQLQueryBuilder<GroupType>(GroupType.class);
        queryBuilder.addEq("iD", urlParams.get("id"));
        return queryBuilder.getUniqueResult();
    }

    public HQLCriterionsForFilter<GroupType> getHQLCriteriaForFilterGroup() {
        GroupTypeQuery query = new GroupTypeQuery();
        HQLCriterionsForFilter<GroupType> criteria = query.getHQLCriterionsForFilter();
        criteria.addQueryModifier(new QueryModifier<GroupType>() {
            @Override
            public void modifyQuery(HQLQueryBuilder<GroupType> hqlQueryBuilder, Map<String, Object> map) {
            }
        });
        return criteria;

    }

    /**
     * Save Group and redirect to edition page
     *
     * @param group Group to save
     * @return Return the redirection link (editPage)
     */
    public String saveGroupAndRedirect(GroupType group) {
        if (group != null) {
            GroupTypeQuery groupTypeQuery = new GroupTypeQuery();
            groupTypeQuery.iD().eq(group.getID());
            GroupType exist = groupTypeQuery.getUniqueResult();
            if (exist == null) {
                entityManager.merge(group);
                entityManager.flush();
                FacesMessages.instance().add("The Group has been successfully created.");
                return "/browser/editGroup.seam?id=" + group.getID();
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "A Group with the id " + group.getID() + " already exists.");
            }

        }
        return null;
    }

    /**
     * Save edited Group
     *
     * @param group Edited group to save
     */
    public void saveEditGroup(GroupType group) {
        if (group != null) {
            entityManager.merge(group);
            entityManager.flush();
            FacesMessages.instance().add("The Group has been successfully edited.");
        } else {
            FacesMessages.instance().add("You have some errors in your Group Object.");
        }
    }

    /**
     * Save edited Group and redirect to edition page
     *
     * @param group Edited group to save
     * @return Return the redirection link (editPage)
     */
    public String saveEditGroupAndRedirect(GroupType group) {
        if (group != null) {
            entityManager.merge(group);
            entityManager.flush();
            FacesMessages.instance().add("The Group has been successfully edited.");
            return "/browser/editGroup.seam?id=" + group.getID();
        }
        return null;
    }

    /**
     * Add link between a group and a value set
     *
     * @param valueSet Value set to link with group
     * @param group Group to link with valueSet
     */
    public void addValueSetToGroup(DescribedValueSet valueSet, GroupType group) {
        // Verify if already exist before add link

        List<String> valuesetList = new ArrayList<String>();
        for (DescribedValueSet valueset : group.getValueSets()) {
            valuesetList.add(valueset.getId());
        }
        if (!valuesetList.contains(valueSet.getId())) {
            // Add the link between group and value set
            valueSet.getGroup().add(group);
            group.getValueSets().add(valueSet);

            // Save database
            group = entityManager.merge(group);
            entityManager.flush();

            FacesMessages.instance().add("Value Set has been successfully added to this group.");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Value Set has already been added to this group.");
        }
    }

    /**
     * Remove the link between a group and a value set
     *
     * @param valueSet Value set to unlink with group
     * @param group Group to unlink with valueSet
     */

    public void removeValueSetToGroup(DescribedValueSet valueSet, GroupType group) {

        DescribedValueSet vs = entityManager.find(DescribedValueSet.class, valueSet.getIdDB());
        GroupType gt = entityManager.find(GroupType.class, group.getIdDB());

        vs.getGroup().remove(gt);
        gt.getValueSets().remove(valueSet);
        group = entityManager.merge(gt);
        entityManager.flush();

        FacesMessages.instance().add("Value Set: " + valueSet.getId() + " successfully removed from the group.");
    }

    /**
     * Delete a group
     *
     * @param group The group to delete
     * @return Redirection link to groupBrowser
     */
    public String deleteGroup(GroupType group) {

        GroupTypeQuery groupTypeQuery = new GroupTypeQuery();
        groupTypeQuery.iD().eq(group.getID());
        GroupType groupToRemove = groupTypeQuery.getUniqueResult();

        GroupType groupToRemove2 = entityManager.find(GroupType.class, groupToRemove.getId());
        entityManager.remove(groupToRemove2);
        entityManager.flush();

        FacesMessages.instance().add("The Group has been deleted.");
        return "/browser/groupBrowser.seam";
    }
}
