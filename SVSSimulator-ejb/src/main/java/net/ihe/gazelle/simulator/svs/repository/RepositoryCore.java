package net.ihe.gazelle.simulator.svs.repository;

import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.svs.model.ToolUsage;
import net.ihe.gazelle.simulator.svs.util.HQLRestrictionSimilarTo;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.DescribedValueSet;
import net.ihe.gazelle.svs.ObjectFactory;
import net.ihe.gazelle.svs.RetrieveMultipleValueSetsRequestType;
import net.ihe.gazelle.svs.RetrieveMultipleValueSetsResponseType;
import net.ihe.gazelle.svs.RetrieveValueSetRequestType;
import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.svs.SVSMarshaller;
import net.ihe.gazelle.svs.ValueSetRequestType;
import net.ihe.gazelle.svs.ValueSetResponseType;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class RepositoryCore {

	public static final String OK = "OK";
	public static final String VERUNK = "VERUNK";
	public static final String NAV = "NAV";
	public static final String INV = "INV";

	private String responseStatus;

	public RepositoryCore() {
		responseStatus = null;
	}

	public RetrieveValueSetResponseType buildResponseToRetrieveValueSet(RetrieveValueSetRequestType request) {
		ValueSetRequestType valueSetRequest = request.getValueSet();
		if (valueSetRequest.getId() != null) {
			EntityManager entityManager = getEntityManager();
			RetrieveValueSetResponseType response = null;
			boolean versionRequested = false;
			boolean langRequested = false;

			// Ignore Leading Zeroes
			valueSetRequest.setId(valueSetRequest.getId().replaceAll("^0+", ""));
			// Add Restriction for ID
			HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(entityManager,
					DescribedValueSet.class);
			queryBuilder.addEq("id", valueSetRequest.getId());

			// Add Restriction for version
			if (valueSetRequest.getVersion() != null && !valueSetRequest.getVersion().isEmpty()) {
				versionRequested = true;
				queryBuilder.addEq("version", valueSetRequest.getVersion());
			}

			if (valueSetRequest.getLang() != null && !valueSetRequest.getLang().isEmpty()) {
				langRequested = true;
				queryBuilder.addEq("conceptList.lang", valueSetRequest.getLang());
			}
			// Get the value set from database
			DescribedValueSet foundValueSet = queryBuilder.getUniqueResult();

			// if version is not empty and no result is found try again without
			// constraint on version
			if (foundValueSet == null && versionRequested) {
				queryBuilder = new HQLQueryBuilder<DescribedValueSet>(entityManager, DescribedValueSet.class);
				queryBuilder.addEq("id", valueSetRequest.getId());
				if (langRequested) {
					queryBuilder.addEq("conceptList.lang", valueSetRequest.getLang());
				}
				foundValueSet = queryBuilder.getUniqueResult();
				if (foundValueSet == null) {
					responseStatus = NAV;
				} else {
					responseStatus = VERUNK;
				}
			} else if (foundValueSet == null) {
				responseStatus = NAV;
			} else {
				responseStatus = OK;
				response = new RetrieveValueSetResponseType();
				ValueSetResponseType valueSetResponse = new ValueSetResponseType();
				valueSetResponse.setId(foundValueSet.getId());
				valueSetResponse.setDisplayName(foundValueSet.getDisplayName());
				valueSetResponse.setVersion(foundValueSet.getVersion());
				if (!langRequested) {
					valueSetResponse.setConceptList(foundValueSet.getConceptList());
				} else {
					for (ConceptListType conceptList : foundValueSet.getConceptList()) {
						if (conceptList.getLang().equals(request.getValueSet().getLang())) {
							valueSetResponse.setConceptList(new ArrayList<ConceptListType>());
							valueSetResponse.getConceptList().add(conceptList);
							break;
						}
					}
				}
				response.setValueSet(valueSetResponse);
			}
			return response;
		} else {
			return null;
		}
	}

	public RetrieveValueSetResponseType buildResponseForSimulator(RetrieveValueSetRequestType request, Boolean random,
			String code) {
		ValueSetRequestType valueSetRequest = request.getValueSet();
		if (valueSetRequest.getId() != null) {
			// Ignore Leading Zeroes
			String valueSetId = valueSetRequest.getId().replaceAll("^0+", "");

			// Get the ValueSet according to the OID in URL
			EntityManager entityManager = getEntityManager();
			HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(entityManager,
					DescribedValueSet.class);
			queryBuilder.addEq("id", valueSetId);
			DescribedValueSet valueSet = queryBuilder.getUniqueResult();
			// If a ValueSet exist
			if (valueSet != null) {
				ConceptListType mainConceptList = null;
				ConceptListType localeConceptList = null;
				// retrieve localized and main concept lists
				for (ConceptListType clt : valueSet.getConceptList()) {
					if (valueSetRequest.getLang() != null && !valueSetRequest.getLang().isEmpty()
							&& clt.getLang().equalsIgnoreCase(valueSetRequest.getLang())) {
						localeConceptList = clt;
					} else if (clt.getMainList() != null && clt.getMainList()) {
						mainConceptList = clt;
					} else {
						continue;
					}
				}
				List<ConceptListType> conceptListToUse = new ArrayList<ConceptListType>();
				if (localeConceptList != null) {
					conceptListToUse.add(localeConceptList);
				} else {
					conceptListToUse.add(mainConceptList);
				}
				valueSet.setConceptList(conceptListToUse);
				// if random is true, randomly take a code in this list
				if (random != null && random) {
					if (valueSet.getConceptList().size() > 0) {
						// Reduce to one random concept of the ConceptList
						Integer conceptListSize = valueSet.getConceptList().get(0).getConcept().size();
						Random randomGenerator = new Random();
						ArrayList<CE> concepts = new ArrayList<CE>();
						concepts.add(valueSet.getConceptList().get(0).getConcept()
								.get(randomGenerator.nextInt(conceptListSize)));
						valueSet.getConceptList().get(0).setConcept(concepts);
					}
				} else if (code != null) {
					// Reduce to one random concept of the ConceptList
					ArrayList<CE> concepts = new ArrayList<CE>();
					for (CE ce : valueSet.getConceptList().get(0).getConcept()) {
						if (ce.getCode().equalsIgnoreCase(code)) {
							concepts.add(ce);
						}
					}
					valueSet.getConceptList().get(0).setConcept(concepts);
				}
				RetrieveValueSetResponseType response = new RetrieveValueSetResponseType();
				ValueSetResponseType valueSetResponse = new ValueSetResponseType();
				valueSetResponse.setId(valueSet.getId());
				valueSetResponse.setDisplayName(valueSet.getDisplayName());
				valueSetResponse.setVersion(valueSet.getVersion());
				for (ConceptListType conceptList : valueSet.getConceptList()) {
					valueSetResponse.setConceptList(new ArrayList<ConceptListType>());
					valueSetResponse.getConceptList().add(conceptList);
				}
				response.setValueSet(valueSetResponse);
				responseStatus = null;
				return response;
			} else {
				responseStatus = NAV;
				return null;
			}
		} else {
			return null;
		}
	}

	public RetrieveMultipleValueSetsResponseType buildResponseToRetrieveMultipleValueSets(
			RetrieveMultipleValueSetsRequestType request) {
		RetrieveMultipleValueSetsResponseType response = new RetrieveMultipleValueSetsResponseType();
		if (request == null) {
			return response;
		}
		EntityManager entityManager = getEntityManager();
		HQLQueryBuilder<DescribedValueSet> queryBuilder = new HQLQueryBuilder<DescribedValueSet>(entityManager,
				DescribedValueSet.class);
		if (request.getID() != null && !request.getID().isEmpty()) {
			// Ignore Leading Zeroes
			request.setID(request.getID().replaceAll("^0+", ""));
			queryBuilder.addEq("id", request.getID());
		}

		// REGEX Restrictions
		if (request.getDisplayNameContains() != null && !request.getDisplayNameContains().isEmpty()) {
			queryBuilder.addRestriction(new HQLRestrictionSimilarTo("displayName", request.getDisplayNameContains()
					.trim()));
		}

		if (request.getSourceContains() != null && !request.getSourceContains().isEmpty()) {
			queryBuilder.addRestriction(new HQLRestrictionSimilarTo("source", request.getSourceContains().trim()));
		}

		if (request.getPurposeContains() != null && !request.getPurposeContains().isEmpty()) {
			queryBuilder.addRestriction(new HQLRestrictionSimilarTo("purpose", request.getPurposeContains().trim()));
		}

		if (request.getDefinitionContains() != null && !request.getDefinitionContains().isEmpty()) {
			queryBuilder.addRestriction(new HQLRestrictionSimilarTo("definition", request.getDefinitionContains()
					.trim()));
		}

		if (request.getGroupContains() != null && !request.getGroupContains().isEmpty()) {
			queryBuilder.addRestriction(new HQLRestrictionSimilarTo("group.displayName", request.getGroupContains()
					.trim()));
		}

		if (request.getGroupOID() != null && !request.getGroupOID().isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.eq("group.iD", request.getGroupOID()));
		}

		if (request.getEffectiveDateBefore() != null) {
			Date date = request.getEffectiveDateBefore().toGregorianCalendar().getTime();
			queryBuilder.addRestriction(HQLRestrictions.le("effectiveDateUtil", date));
		}

		if (request.getEffectiveDateAfter() != null) {
			Date date = request.getEffectiveDateAfter().toGregorianCalendar().getTime();
			queryBuilder.addRestriction(HQLRestrictions.ge("effectiveDateUtil", date));
		}

		if (request.getExpirationDateBefore() != null) {
			Date date = request.getExpirationDateBefore().toGregorianCalendar().getTime();
			queryBuilder.addRestriction(HQLRestrictions.le("expirationDateUtil", date));
		}

		if (request.getExpirationDateAfter() != null) {
			Date date = request.getExpirationDateAfter().toGregorianCalendar().getTime();
			queryBuilder.addRestriction(HQLRestrictions.ge("expirationDateUtil", date));
		}

		if (request.getCreationDateBefore() != null) {
			Date date = request.getCreationDateBefore().toGregorianCalendar().getTime();
			queryBuilder.addRestriction(HQLRestrictions.le("creationDateUtil", date));
		}

		if (request.getCreationDateAfter() != null) {
			Date date = request.getCreationDateAfter().toGregorianCalendar().getTime();
			queryBuilder.addRestriction(HQLRestrictions.ge("creationDateUtil", date));
		}

		if (request.getRevisionDateBefore() != null) {
			Date date = request.getRevisionDateBefore().toGregorianCalendar().getTime();
			queryBuilder.addRestriction(HQLRestrictions.le("revisionDateUtil", date));
		}

		if (request.getRevisionDateAfter() != null) {
			Date date = request.getRevisionDateAfter().toGregorianCalendar().getTime();
			queryBuilder.addRestriction(HQLRestrictions.ge("revisionDateUtil", date));
		}
		List<DescribedValueSet> valueSetsDatabase = queryBuilder.getListNullIfEmpty();

		if (valueSetsDatabase != null) {
			responseStatus = OK;
			ObjectFactory factory = new ObjectFactory();
			for (DescribedValueSet vs : valueSetsDatabase) {
				// SVS-78 - if more than one conceptList is present in this DescribedValueSet, split each ConceptList in a new DescribedValueSet
				if (vs.getConceptList().size() > 1) {
					for (ConceptListType cl : vs.getConceptList()) {
						DescribedValueSet newDescribedValueSet = new DescribedValueSet();
						List<ConceptListType> conceptList = new ArrayList<ConceptListType>();
						conceptList.add(cl);
						newDescribedValueSet.setConceptList(conceptList);
						newDescribedValueSet.setId(vs.getId());
						newDescribedValueSet.setVersion(vs.getVersion());
						newDescribedValueSet.setDisplayName(vs.getDisplayName());
						newDescribedValueSet.setSource(vs.getSource());
						newDescribedValueSet.setSourceURI(vs.getSourceURI());
						newDescribedValueSet.setPurpose(vs.getPurpose());
						newDescribedValueSet.setDefinition(vs.getDefinition());
						newDescribedValueSet.setType(vs.getType());
						newDescribedValueSet.setBinding(vs.getBinding());
						newDescribedValueSet.setStatus(vs.getStatus());
						if (vs.getGroup() != null) {
							newDescribedValueSet.setGroup(vs.getGroup());
						}
						response.addGroup(factory
								.createRetrieveMultipleValueSetsResponseTypeDescribedValueSet(newDescribedValueSet));
					}
				} else {
					response.addGroup(factory.createRetrieveMultipleValueSetsResponseTypeDescribedValueSet(vs));
				}
			}
		}
		return response;
	}

	private EntityManager getEntityManager() {
		return EntityManagerService.provideEntityManager();
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void saveToolUsage(String ip) {
		EntityManager entityManager = getEntityManager();
		HQLQueryBuilder<ToolUsage> query = new HQLQueryBuilder<ToolUsage>(entityManager, ToolUsage.class);
		query.addEq("callerIp", ip);
		ToolUsage usage = query.getUniqueResult();
		if (usage == null) {
			usage = new ToolUsage(ip);
		} else {
			usage.setLastCall(new Date());
			usage.setNbOfCalls(usage.getNbOfCalls() + 1);
		}
		entityManager.merge(usage);
		entityManager.flush();
	}

	public void saveReceivedTransactionInstance(Object request, Object response, String transactionKeyword, String sut,
			String bindingType, EntityManager entityManager) {
		TransactionInstance instance = new TransactionInstance();
		instance.setTimestamp(new Date());
		if (request != null) {
			instance.getRequest().setContent(SVSMarshaller.marshall(request));
			if (transactionKeyword.equals("ITI-48")) {
				instance.getRequest().setType("RetrieveValueSetRequest (" + bindingType + ")");
			} else {
				instance.getRequest().setType("RetrieveMultipleValueSetsRequest (" + bindingType + ")");
			}
		}
		if (response != null) {
			instance.getResponse().setContent(SVSMarshaller.marshall(response));
			if (transactionKeyword.equals("ITI-48")) {
				instance.getResponse().setType("RetrieveValueSetResponse (" + bindingType + ")");
			} else {
				instance.getResponse().setType("RetrieveMultipleValueSetsResponse (" + bindingType + ")");
			}
		} else {
			instance.getResponse().setContent((String) null);
			instance.getResponse().setType(responseStatus);
		}
		instance.getRequest().setIssuer(sut);
		instance.getResponse().setIssuer("SVS Simulator");
		Actor consumer = Actor.findActorWithKeyword("VALUE_SET_CONSUMER", entityManager);
		Actor repository = Actor.findActorWithKeyword("VALUE_SET_REPOSITORY", entityManager);
		Transaction transaction = Transaction.GetTransactionByKeyword(transactionKeyword, entityManager);
		instance.getRequest().setIssuingActor(consumer);
		instance.getResponse().setIssuingActor(repository);
		instance.setTransaction(transaction);
		instance.setDomain(Domain.getDomainByKeyword("ITI", entityManager));
		instance.setSimulatedActor(repository);
		entityManager.merge(instance);
		entityManager.flush();
	}

	public void setReponseStatus(String status) {
		this.responseStatus = status;
	}
}
