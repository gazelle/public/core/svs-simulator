INSERT INTO app_configuration (id, variable, value) VALUES (1, 'application_issue_tracker_url', 'https://gazelle.ihe.net/jira/browse/SVS');
INSERT INTO app_configuration (id, variable, value) VALUES (2, 'application_name', 'SVS Simulator');
INSERT INTO app_configuration (id, variable, value) VALUES (3, 'application_release_notes_url', 'https://gazelle.ihe.net/jira/browse/SVS#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel');
INSERT INTO app_configuration (id, variable, value) VALUES (4, 'application_time_zone', 'UTC+01');
INSERT INTO app_configuration (id, variable, value) VALUES (5, 'svs_repository_url', 'https://gazelle.ihe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (6, 'documentation_url', 'https://gazelle.ihe.net/content/svs-simulator');
INSERT INTO app_configuration (id, variable, value) VALUES (7, 'application_url', 'http://localhost:8080/SVSSimulator');
INSERT INTO app_configuration (id, variable, value) VALUES (8, 'esvs_xsd_location', 'https://gazelle.ihe.net/XSD/IHE/SVS/ESVS-20141120.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (9, 'message_permanent_link', 'http://localhost:8080/SVSSimulator/messages/messageDisplay.seam?id=');
INSERT INTO app_configuration (id, variable, value) VALUES (10, 'svs_xsd_location', 'https://gazelle.ihe.net/XSD/IHE/SVS/SVS.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (11, 'link_repository_http', 'https://gazelle.ihe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (12, 'link_repository_soap', 'https://gazelle.ihe.net/ValueSetRepository/SoapRepository?wsdl');
INSERT INTO app_configuration (id, variable, value) VALUES (13, 'results_xsl_location', 'https://gazelle.ihe.net/xsl/svsDetailedResult.xsl');
INSERT INTO app_configuration (id, variable, value) VALUES (14, 'ip_login', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (15, 'ip_login_admin', '127\.0\.0\.1');
INSERT INTO app_configuration (id, variable, value) VALUES (17, 'cas_enabled', 'true');
INSERT INTO app_configuration (id, variable, value) VALUES (18, 'application_admin_email', 'abe@kereval.com');
INSERT INTO app_configuration (id, variable, value) VALUES (19, 'application_admin_name', 'Anne-Gaelle BERGE');
INSERT INTO app_configuration (id, variable, value) VALUES (20, 'application_admin_title', 'Application Administrator');
INSERT INTO app_configuration (id, variable, value) VALUES (21, 'NUMBER_OF_ITEMS_PER_PAGE', '20');
INSERT INTO app_configuration (id, variable, value) VALUES (22, 'ignore_validation_in_import', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (23, 'show_simulator_and_validator', 'true');
INSERT INTO app_configuration (id, variable, value) VALUES (24, 'link_to_cgu', 'cgu link that need to be changed');

SELECT pg_catalog.setval('app_configuration_id_seq', 24, true);

INSERT INTO tf_transaction(id, description, keyword, name) VALUES (1, 'Retrieve Value Set', 'ITI-48', 'Retrieve value set');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (2, 'Retrieve Multiple Value Sets', 'ITI-60', 'Retrieve multiple value sets');
SELECT pg_catalog.setval('tf_transaction_id_seq', 2, true);

INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (1, 'Value set repository', 'VALUE_SET_REPOSITORY', 'Value set repository', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (2, 'Value set consumer', 'VALUE_SET_CONSUMER', 'Value set consumer', false);
SELECT pg_catalog.setval('tf_actor_id_seq', 2 , true);

INSERT INTO tf_integration_profile (id, description, keyword, name) VALUES (1, 'Sharing Value Sets', 'SVS', 'Sharing Value Sets');
SELECT pg_catalog.setval('tf_integration_profile_id_seq', 1, true);

INSERT INTO tf_integration_profile_option(id, description, keyword, name) VALUES (1, 'SOAP Binding', 'SOAP_BINDING', 'SOAP Binding');
INSERT INTO tf_integration_profile_option(id, description, keyword, name) VALUES (2, 'HTTP Binding', 'HTTP_BINDING', 'HTTP Binding');
INSERT INTO tf_integration_profile_option(id, description, keyword, name) VALUES (3, 'Retrieve Multiple value sets', 'RETR_MULT_VALUE_SETS', 'Retrieve Multiple value sets');
SELECT pg_catalog.setval('tf_integration_profile_option_id_seq', 3, true);

INSERT INTO tf_actor_integration_profile(id, actor_id, integration_profile_id) VALUES (1, 1, 1);
INSERT INTO tf_actor_integration_profile(id, actor_id, integration_profile_id) VALUES (2, 2, 1);
SELECT pg_catalog.setval('tf_actor_integration_profile_id_seq', 2, true);

INSERT INTO tf_actor_integration_profile_option(id, actor_integration_profile_id, integration_profile_option_id) VALUES (1, 1, 3);
INSERT INTO tf_actor_integration_profile_option(id, actor_integration_profile_id, integration_profile_option_id) VALUES (2, 2, 1);
INSERT INTO tf_actor_integration_profile_option(id, actor_integration_profile_id, integration_profile_option_id) VALUES (3, 2, 2);
INSERT INTO tf_actor_integration_profile_option(id, actor_integration_profile_id, integration_profile_option_id) VALUES (4, 2, 3);
SELECT pg_catalog.setval('tf_actor_integration_profile_option_id_seq', 4, true);

INSERT INTO tf_domain (id, description, keyword, name) VALUES (1, 'ITI', 'ITI', 'IT-Infrastructure');
SELECT pg_catalog.setval('tf_domain_id_seq', 1, true);

INSERT INTO tf_domain_integration_profiles (integration_profile_id, domain_id) VALUES (1, 1);

INSERT INTO affinity_domain(id, keyword, label_to_display, profile) VALUES (1,'IHE','IHE','All');
INSERT INTO usage_metadata(id, action, affinity_id, transaction_id, keyword) VALUES (1,'ITI-48',1,1,'ITI-48');
INSERT INTO usage_metadata(id, action, affinity_id, transaction_id, keyword) VALUES (2,'ITI-60',1,2,'ITI-60');
