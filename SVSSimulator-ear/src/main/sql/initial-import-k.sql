INSERT INTO app_configuration (id, variable, value) VALUES (1, 'application_issue_tracker_url', 'http://gazelle.ihe.net/jira/browse/SVS');
INSERT INTO app_configuration (id, variable, value) VALUES (2, 'application_name', 'SVS Simulator');
INSERT INTO app_configuration (id, variable, value) VALUES (3, 'application_release_notes_url', 'http://gazelle.ihe.net/jira/browse/SVS#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel');
INSERT INTO app_configuration (id, variable, value) VALUES (4, 'application_time_zone', 'UTC+01');
INSERT INTO app_configuration (id, variable, value) VALUES (5, 'svs_repository_url', 'http://k-project.ihe-europe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (6, 'documentation_url', 'http://gazelle.ihe.net/content/svs-simulator');
INSERT INTO app_configuration (id, variable, value) VALUES (7, 'application_url', 'http://k-project.ihe-europe.net/SVSSimulator');
INSERT INTO app_configuration (id, variable, value) VALUES (8, 'esvs_xsd_location', 'http://gazelle.ihe.net/xsd/ESVS-20100726.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (9, 'message_permanent_link', 'http://k-project.ihe-europe.net/SVSSimulator/messages/messageDisplay.seam?id=');
INSERT INTO app_configuration (id, variable, value) VALUES (10, 'svs_xsd_location', 'http://gazelle.ihe.net/xsd/SVS.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (11, 'link_repository_http', 'http://gazelle.ihe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (12, 'link_repository_soap', 'http://k-project.ihe-europe.net/ValueSetRepository/SoapRepository?wsdl');
INSERT INTO app_configuration (id, variable, value) VALUES (13, 'results_xsl_location', 'http://gazelle.ihe.net/xsl/svsDetailedResult.xsl');
INSERT INTO app_configuration (id, variable, value) VALUES (14, 'ip_login', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (15, 'ip_login_admin', '.*');
INSERT INTO app_configuration (id, variable, value) VALUES (16, 'cas_url', 'http://gazelle.ihe.net/cas/');
INSERT INTO app_configuration (id, variable, value) VALUES (17, 'application_works_without_cas', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (18, 'ignore_validation_in_import', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (19, 'show_simulator_and_validator', 'true');

SELECT pg_catalog.setval('app_configuration_id_seq', 20, true);

INSERT INTO tf_transaction(id, description, keyword, name) VALUES (1, 'Retrieve Value Set', 'ITI-48', 'Retrieve value set');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (2, 'Retrieve Multiple Value Sets', 'ITI-60', 'Retrieve multiple value sets');
SELECT pg_catalog.setval('tf_transaction_id_seq', 2, true);

INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (1, 'Value set repository', 'VALUE_SET_REPOSITORY', 'Value set repository', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (2, 'Value set consumer', 'VALUE_SET_CONSUMER', 'Value set consumer', false);
SELECT pg_catalog.setval('tf_actor_id_seq', 2 , true);

INSERT INTO tf_integration_profile (id, description, keyword, name) VALUES (1, 'Sharing Value Sets', 'SVS', 'Sharing Value Sets');
SELECT pg_catalog.setval('tf_integration_profile_id_seq', 1, true);

INSERT INTO tf_integration_profile_option(id, description, keyword, name) VALUES (1, 'SOAP Binding', 'SOAP_BINDING', 'SOAP Binding');
INSERT INTO tf_integration_profile_option(id, description, keyword, name) VALUES (2, 'HTTP Binding', 'HTTP_BINDING', 'HTTP Binding');
INSERT INTO tf_integration_profile_option(id, description, keyword, name) VALUES (3, 'Retrieve Multiple value sets', 'RETR_MULT_VALUE_SETS', 'Retrieve Multiple value sets');
SELECT pg_catalog.setval('tf_integration_profile_option_id_seq', 3, true);

INSERT INTO tf_actor_integration_profile(id, actor_id, integration_profile_id) VALUES (1, 1, 1);
INSERT INTO tf_actor_integration_profile(id, actor_id, integration_profile_id) VALUES (2, 2, 1);
SELECT pg_catalog.setval('tf_actor_integration_profile_id_seq', 2, true);

INSERT INTO tf_actor_integration_profile_option(id, actor_integration_profile_id, integration_profile_option_id) VALUES (1, 1, 3);
INSERT INTO tf_actor_integration_profile_option(id, actor_integration_profile_id, integration_profile_option_id) VALUES (2, 2, 1);
INSERT INTO tf_actor_integration_profile_option(id, actor_integration_profile_id, integration_profile_option_id) VALUES (3, 2, 2);
INSERT INTO tf_actor_integration_profile_option(id, actor_integration_profile_id, integration_profile_option_id) VALUES (4, 2, 3);
SELECT pg_catalog.setval('tf_actor_integration_profile_option_id_seq', 4, true);

INSERT INTO tf_domain (id, description, keyword, name) VALUES (1, 'ITI', 'ITI', 'IT-Infrastructure');
SELECT pg_catalog.setval('tf_domain_id_seq', 1, true);

INSERT INTO tf_domain_integration_profiles (integration_profile_id, domain_id) VALUES (1, 1);

