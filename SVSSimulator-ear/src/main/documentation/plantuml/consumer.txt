@startuml
hide footbox
title Sharing value sets
participant CONSUMER as "VALUE_SET_CONSUMER (SVSSimulator)" #99FF99
participant REPOSITORY as "VALUE_SET_REPOSITORY (SUT)"
group Retrieve Value Set (ITI-48)
CONSUMER -> REPOSITORY : RetrieveValueSetRequest
activate REPOSITORY
REPOSITORY --> CONSUMER : RetrieveValueSetResponse
deactivate REPOSITORY
end
group Retrieve Multiple Value Sets (ITI-60)
CONSUMER -> REPOSITORY : RetrieveMultipleValueSetsRequest
activate REPOSITORY
REPOSITORY --> CONSUMER : RetrieveMultipleValueSetsResponse
deactivate REPOSITORY
end
@enduml