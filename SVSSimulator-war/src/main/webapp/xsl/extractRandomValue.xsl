<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="2.0" xmlns:math="http://www.jclark.com/xt/java/java.lang.Math">

	<xsl:output indent="yes" method="xml" />
	<xsl:param name="randomIndex"
		select="ceiling(math:random() * count(//node()[name() = 'Concept']))" />

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()">
				<xsl:with-param name="randomIndex" />
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="node()[name() = 'Concept']">
		<xsl:if test="((count(preceding-sibling::*) + 1) = $randomIndex)">
			<xsl:copy>
				<xsl:apply-templates select="@*|node()" />
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<xsl:template match="comment()" />
</xsl:stylesheet>