<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:output encoding="utf-8" indent="yes" method="html"
		omit-xml-declaration="yes" />

	<xsl:template match="RetrieveValueSetRequest">
		<p>
			<xsl:text>/RetrieveValueSet?</xsl:text>
			<xsl:if test="count(ValueSet/@id) = 1">
				<xsl:text>id=</xsl:text>
				<xsl:value-of select="ValueSet/@id" />
			</xsl:if>
			<xsl:if
				test="count(ValueSet/@id) &gt; 0 and string-length(ValueSet/@xml:lang) &gt; 0">
				<xsl:text>&amp;</xsl:text>
			</xsl:if>
			<xsl:if test="string-length(ValueSet/@xml:lang) &gt; 0">
				<xsl:text>lang=</xsl:text>
				<xsl:value-of select="ValueSet/@xml:lang" />
			</xsl:if>
			<xsl:if
				test="count(ValueSet/@version) &gt; 0 and (string-length(ValueSet/@xml:lang) &gt; 0 or string-length(ValueSet/@version) &gt; 0)">
				<xsl:text>&amp;</xsl:text>
			</xsl:if>
			<xsl:if test="string-length(ValueSet/@version) &gt; 0">
				<xsl:text>version=</xsl:text>
				<xsl:value-of select="ValueSet/@version" />
			</xsl:if>
		</p>
	</xsl:template>
	<xsl:template match="RetrieveMultipleValueSetsRequest">
		<p>
			<xsl:text>/RetrieveMultipleValueSetsRequest?</xsl:text>
			<xsl:for-each select="child::node()">
				<xsl:if test="not(name() = '')">
					<xsl:if test="position() &gt; 2">
						<xsl:text>&amp;</xsl:text>
					</xsl:if>
					<xsl:value-of select="name()" />
					<xsl:text>=</xsl:text>
					<xsl:value-of select="." />
				</xsl:if>
			</xsl:for-each>
		</p>
	</xsl:template>
</xsl:stylesheet>
