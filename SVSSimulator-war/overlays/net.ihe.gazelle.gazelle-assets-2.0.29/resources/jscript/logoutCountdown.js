var nextLogoutDate = null;
var countdownLogoutTimer = null;
var warnTimeTimer = 10 * 60 * 1000;
var localServerTimeDiffDeploy;

function padTwoCharsDeploy(number) {
	var str = '' + number;
	while (str.length < 2) {
		str = '0' + str;
	}
	return str;
}

function updateCountdownLogout() {
	if (nextLogoutDate != null) {
		var nowDate = new Date();
		var nowTime = nowDate.getTime();
		var nowTimeServer = nowTime - localServerTimeDiffDeploy;

		var diffTime = (nextLogoutDate.getTime() - nowTimeServer) / 1000;
		if (diffTime > 0) {
			var minutesLeft = padTwoCharsDeploy(Math.floor(diffTime / 60));
			var secondsLeft = padTwoCharsDeploy(Math.floor(diffTime
					- (60 * minutesLeft)));
			var timeLeft = minutesLeft + ":" + secondsLeft;
			document.getElementById('lougoutCountdown').innerHTML = timeLeft;
			setTimeout("updateCountdownLogout();", 1000);
		} else {
			document.getElementById('logoutScheduled').className = 'schedule-invisible';
			document.getElementById('logoutExecuted').className = 'schedule-visible gzl-alert gzl-alert-red';
		}
	}
}

function startCountDownLogOut(nextLogoutTime, serverDateTime) {
	var nowDate = new Date();
	var nowTime = nowDate.getTime();

	localServerTimeDiffDeploy = nowTime - serverDateTime;

	var diffTime = nextLogoutTime - serverDateTime;

	if (diffTime > 0 && diffTime < warnTimeTimer) {

		if (nextLogoutDate == null
				|| nextLogoutDate.getTime() != nextLogoutTime) {
			nextLogoutDate = new Date();
			nextLogoutDate.setTime(nextLogoutTime);
		}

		updateCountdownLogout();
		setTimeout("updateCountdownLogout();", 1000);
		document.getElementById('logoutScheduled').className = 'schedule-visible gzl-alert gzl-alert-red';
	} else {
		stopCountDownLogout();
	}

}

function stopCountDownLogout() {
	nextLogoutDate = null;
	document.getElementById('logoutScheduled').className = 'schedule-invisible';
}