function newOnloadFunction(event) {
    colorRowsOfTable("globalform:iheImplementationResultsTable");
}

function colorRowsOfTable(idTable) {
    var table = document.getElementById(idTable);
    if (table != null) {
        var numberOfRows = table.rows.length;
        var stringStatus = "";
        for (var i = 1; i < numberOfRows; i = i + 2) {
            colorRow(table.rows[i]);
        }
    }
}

function colorRow(idRow) {
    var currentRow = idRow;
    if (currentRow) {
        var tdToUse = currentRow.cells[currentRow.cells.length - 2];
        if (tdToUse) {
            var firstDiv = tdToUse.firstChild;
            if (firstDiv) {
                var hidden = firstDiv.firstChild;
                if (hidden) {
                    if (hidden.value == 1) // pass
                        currentRow.style.background = "#CCFFCC";
                    else if (hidden.value == 2) // fail
                        currentRow.style.background = "#FFCCCC";
                    else if (hidden.value == 3) // at risk
                        currentRow.style.background = "#FFCC33";
                    else
                    // other cases but not null
                        currentRow.style.background = "#E0E0E0";
                }
            }
        }
    }
}

gazelleAddToBodyOnLoad(newOnloadFunction);

addAjaxStopFunction(function () {
    colorRowsOfTable("globalform:iheImplementationResultsTable");
});
